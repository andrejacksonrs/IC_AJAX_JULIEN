# Final Tasks

- [ ] Create tutorial about surface evolver physical quantities and unit system
- [ ] Check the English of the documents produced
- [ ] Fix Contact Line Theory document issues
- [ ] Erase unecessary files of the project 
- [ ] Prepare and Record Congresso UFBA presentation to train for it
- [ ] Generate PDF files of the works done
- [ ] Prepare Gitlab and git workshop
- [ ] Generate pdf files of overleaf reports