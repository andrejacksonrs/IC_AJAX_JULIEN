"""

Nonlinear fit

Author: André Jackson Ramos Simões

"""
#Libraries used
import numpy as np
import matplotlib.pyplot as plt
import scipy
from scipy import optimize

#Dataset
X = np.array([1,2,3,4,5])
Y = np.array([1,4.2,8.5,17,24])

#Nonlinear fitting, with a power function, but you can write any nonlinear function
def f(x,a,b,c):
    return a*pow(x,b) + c

guess = [1,1,1]

params, params_covariance = scipy.optimize.curve_fit(f, X, Y, guess)
params

a = params[0]
b = params[1]
c = params[2]

#Formating the graph and plotting
plt.title('Title', fontsize = 15)
plt.xlabel('x axis')
plt.ylabel('y axis')

plt.scatter(X,Y, color = "black", label = 'Data')

#Redefine X array with more values to plot a smoother version of the function f
X = np.linspace(1.0,5.0,100)
plt.plot(X, f(X,a,b,c), color="blue", label = 'nonlinear Fit')

#Printing the fitting constants a and b in the console
print("The coeficient a is equal to:")
print(a)
print("And the coeficient b is equal to:")
print(b)
print("And the coeficient c is equal to:")
print(c)

#Plotting legend
plt.legend(loc='upper left', prop={'size': 12})

#Export
#plt.savefig('nonlinearfit.png')









