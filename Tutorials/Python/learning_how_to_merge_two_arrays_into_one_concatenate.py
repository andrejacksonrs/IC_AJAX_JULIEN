# -*- coding: utf-8 -*-
"""
Learning how to merge two arrays into one, concatenate
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy
from scipy import optimize
import pandas as pd

v1 = np.array([1,2,3,4,5])
v2 = np.array([11,12,13,14,15])

v1 = np.concatenate((-v1,v1))
v2 = np.concatenate((v2,v2))

