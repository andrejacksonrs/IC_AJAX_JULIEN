""" 
Writting and Plotting 2D data

Author: André Jackson Ramos Simões
"""

import numpy as np
import matplotlib.pyplot as plt

#writting data
x = np.array([1,2,3,4,5])

y = np.array([5,4,3,2,1])

#plotting data
plt.scatter(x,y, color = 'black', label = 'points')

plt.plot(x,y, color = 'blue', label = 'line')

#Titles, legends, and saving the fig
plt.legend(loc='upper right')

plt.xlabel('x axis', size = 10)

plt.ylabel('y axis', size = 10)

plt.title('This is the title of the graph', size = 15)

#plt.savefig('2Dplotname.png')
