"""

Linear fitting

Author: André Jackson Ramos Simões

"""
#Libraries used
import numpy as np
import matplotlib.pyplot as plt
import scipy
from scipy import optimize

#Dataset
X = np.array([1,2,3,4,5])
Y = np.array([1,5,2,9,10])

#Linear fitting
def f(x,a,b):
    return a*x + b

guess = [1,1]

params, params_covariance = scipy.optimize.curve_fit(f, X, Y, guess)
params

a = params[0]
b = params[1]

#Plotting the graph

#Formating the graph and plotting
plt.title('Title', fontsize = 15)
plt.xlabel('x axis')
plt.ylabel('y axis')

plt.scatter(X,Y, color = "black", label = 'data')
plt.plot(X, f(X,a,b), color="red", label = 'fit')

#Printing the fitting constants a and b 
print("The coeficient a is equal to:")
print(a)
print("And the coeficient b is equal to:")
print(b)

#Plotting legend
plt.legend(loc='upper left')

#Export
#plt.savefig('Linear fitting of nameofthefile.png')







