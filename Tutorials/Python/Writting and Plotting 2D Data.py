""" 
Writting and Plotting 2D data

Author: André Jackson Ramos Simões
"""

#Very rich scientific Libraries chosen to write and plot the 2D data
import numpy as np
import matplotlib.pyplot as plt

#----------------------------------Writing data--------------------------------
#Writting x axis values
x = np.array([1,2,3,4,5])

#Writting y axis values
y = np.array([5,4,3,2,1])


#---------------------------------Plotting data--------------------------------
#Scatter Plot
plt.scatter(x,y, color = 'black', label = 'points')

#Line plot
plt.plot(x,y, color = 'blue', label = 'line')


#--------------------------Inserting titles and legends------------------------
#Legend of the plots
plt.legend(loc='upper right')

#Title of x axis:
plt.xlabel('x axis', size = 10)

#Title of y axis:
plt.ylabel('y axis', size = 10)

#Title of the Graph
plt.title('This is the title of the graph', size = 15)

"""To export the plot to the same folder of this script, just use the command 
below by removing the # symbol. Also, you can choose the format by rewriting
the file name, for example if you want it to be exported in .jpeg just write
2Dplotname.jpeg inspite of 2Dplotname.png. You can export in pdf, svg, etc. """

#plt.savefig('2Dplotname.png')