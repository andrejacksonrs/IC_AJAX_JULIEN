"""
How to select y = k of a 3d dataset
"""
import numpy as np
import matplotlib.pyplot as plt
import scipy
from scipy import optimize
import pandas as pd

x = np.array([1.0,2.0,3.0,4.0,5.0])
y = np.array([0.0,1.0,0.0,1.0,0.0])
z = np.array([1.0,2.0,3.0,4.0,5.0])

s = np.size(x)
print(x)

X = np.array([])
#print(X)

Z = np.array([])
#print(Z)


for i in range(s):
    if y[i] == 0:
        X = np.append(X,x[i])
        Z = np.append(Z,z[i])
      


