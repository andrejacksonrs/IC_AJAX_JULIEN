"""
Writting and Plotting 3D data

Author: André Jackson Ramos Simões
"""

#Libraries used
import numpy as np
import matplotlib.pyplot as plt

#Dataset
X = np.array([1,2,3,4,5])
Y = np.array([1,2,3,4,5])
Z = np.array([1,2,3,4,5])

#Plot
fig = plt.figure()
ax = fig.gca(projection='3d')
ax.scatter(X, Y, Z)

#Formating of the graph
plt.title('Title', fontsize = 15)

ax.set_xlabel('X axis')
ax.set_ylabel('Y axis')
ax.set_zlabel('Z axis')

ax.set_xticks(np.linspace(1,5,5))
ax.set_yticks(np.linspace(1,5,5))
ax.set_zticks(np.linspace(1,5,5))

#To Export, just remove the # of the next command and run the code
#plt.savefig('3Dplot1.pdf')
