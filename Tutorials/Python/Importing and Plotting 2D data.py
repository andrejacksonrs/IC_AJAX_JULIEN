""" 
Importing and Plotting 2D data

Author: André Jackson Ramos Simões
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

#Reading data from the a file
table = pd.read_csv("table.txt", sep = "\t").to_numpy()

#Defining coordinates of the points based on the table file
x = table[:,0]
y = table[:,1]

#plotting data
plt.scatter(x,y, color = 'black', label = 'points')

plt.plot(x,y, color = 'blue', label = 'line')

#Titles, legends, and saving the fig
plt.legend(loc='upper left')

plt.xlabel('x axis', size = 10)

plt.ylabel('y axis', size = 10)

plt.title('This is the title of the graph', size = 15)

#plt.savefig('2Dplotname.png')
