# -*- coding: utf-8 -*-
"""
Learning interpolation with python
"""
import numpy as np
import matplotlib.pyplot as plt
import scipy
from scipy import optimize
import pandas as pd

#creating measurements 
x = np.linspace(0, 1, 10)
dx = (np.random.random(10)*2 - 1) * 1e-1
y = np.sin(2 * np.pi * x) + dx

#interpolation of measurements
from scipy.interpolate import interp1d as intp
linear_interp = intp(x,y)


#Here is the control of the  interpolated points, the last number in the 
#linspace defines how many interpolations will be done, and the 2 first numbers
#define the interval in witch the interpolation will occur.
xvalues = np.linspace(0.4, 0.6, 10)

linear_results = linear_interp(xvalues)

plt.scatter(x,y, label = 'measurements')
plt.scatter(xvalues,linear_results, label = 'interpolation', color = 'red')

plt.legend()