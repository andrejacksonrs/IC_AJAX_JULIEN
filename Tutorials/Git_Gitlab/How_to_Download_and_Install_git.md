# GitLab and git Tutorial - First Steps

##### Author: André Jackson Ramos Simões

## Installing and configuring git

1. Go to https://git-scm.com/downloads to download git.

2. After getting git downloaded and installed, let's verify that everything is ok with the installation by opening git bash that should come with the installation of git for windows. After opening git bash, type: 

   `git` - to see a big list of commands

   and

    `git version` - to see its version

3. Now that git is installed, let's configure it. First, to configure your user name, type:

   `git config --global user.name "write your name here"`

   In addition, to configure your email, type:

   `git config --global user.email "write your email here"`

4. To see the current configurations, type:

   `git config --global --list` 

   or a more abbreviated version of that command: 

   `git config --global -l`

5. If you want to change your configurations, you can delete your user name by typing:

   `git config --global --unset user.name` 

   and your email by: 

   `git config --global --unset user.email`

   To check if those configurations were really removed, you can always type:

   `git config --global -l` 

   to see the actual configurations. Now, git is installed and configured.

## Starting a local project

1. Now that git is installed and configured, we can create a local git repository in your pc. To do that, create a folder in your desktop for example, with the name "Project". Then, we must now access this folder with the command prompt or Git Bash by typing: 

   `cd desktop` 

   then 

   `cd Project`

   This command "cd" stands for change directory, and it allows us to access different folders of the pc. If you want to come back to desktop folder for some reason, just type: 

   `cd -`

2. Now, inside the Project folder, typing 

   `git init` 

   will start a local git repository in this folder. If you see the hidden files of the project folder you will find a hidden folder called .git, there are the files that will make git care about every change you do in the project. 

3. If you want to go back and do not have a git repo (repository) on your project folder anymore, just type:

   `rm - rf .git/` 

4. To see the files that are inside the project folder you can type:

   `ls - la`

   That show all files, including hidden ones. But if you do not want to see the hidden files, you can just type:

   `ls`

## Commit changes to the project

1. First, add a .txt file, for example, to the project folder and type

   `git status`

   now, in gitbash (or terminal), git shows to us that it's seeing a new guy over there as untracked.

2. To track this file with git, i.e., to register its evolution, modifications, and everything else, just type:

   `git add nameofthefile.txt`

   It will be now what we call, a staged file, it means that git is caring about it and is prepared to register its presence in what we call a commit. To unstage the file, if you want, you can type:

   `git rm --cached nameofthefile.txt`

3. Now that git is tracking this file, changes inside it would be detected by git, and we could see them typing `git status`. To register the state of the actual project we commit the changes, and we do that by typing:

   `git commit -m "C0 - first commit, added a random .txt file"`

   in that command, the first part "git commit" stands for the fact that we want to commit changes in the project. the part "-m" indicates we will write a message specially for this commit, to identify it and see what was going on when we did that changes. After "-m", we write that message in quotes "". A good habit is to write the number of the commit like done with "C0" is the commit number 0. This is good to localize the number of modifications done in the project and localize the commit itself by its number despite of the activity done in it.

4. To see all the commits done in the project, type

   `git log`

   This opens in Git Bash (or terminal) a full list of all the commits, commented and everything else. To quit this list, just type the letter "q". It is very common to have a lot of commits in the project, in a way that sometime we just want to see the last commit, or the last 2 commits. To do that, type:

   `git log -1` for the last commit

   `git log -2` for the last 2 commits

   `git log --oneline` for all the commits but resumed in one line

   `git log -1 --oneline` for the last commit resumed in one line, and so on.

5. To see exactly what happened in a certain commit, copy its rash number (the number that appears after the word commit when you log a commit) and type:

   `git show 123891927491827sdad81293817`

   where "123891927491827sdad81293817" would be the rash number of the commit you want to see what was done

6. Sometimes, we want to change the message of the commit we've just done. To do that, change the message of the last commit, we type:

   `git commit --amend -m "C0 - First commit, I added a .txt file and i'm learning to work with git :D"`

   where "C0 - First commit, I added a .txt file and i'm learning to work with git :D" would be the new message.



## References

1. Udemy: Git e GitHub - Básico e Intermediário (https://www.udemy.com/course/git-e-github-basico-e-intermediario) 