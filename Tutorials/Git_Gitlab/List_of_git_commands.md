# List of git commands

##### Author: André Jackson Ramos Simões

## 1. Installing and Configuring git 

1. To see a list of commands: `git`
2. To see git's version: `git version`
3. To set username: `git config --global user.name "write your name here"`
4. To set user's email: `git config --global user.email "write your email here"`
5. To delete your username: `git config --global --unset user.name`
6. To delete your email: `git config --global --unset user.email`
7. To see the actual configurations: `git config --global --list` or `git config --global -l`


## 2. Starting a local project

1. To start a git repository in a local folder: `git init`
2. To delete a git repository created in a local folder: `rm -rf .git/`
3. To see what git is seeing, like recent modifications or files added to the repository for example: `git status` or `git status -u` to see inside folders
4.  To see the files inside a folder: `ls` or `ls -la` to see hidden files too (including git repository files that are installed when `git init` is used)
1. To clone an existing project on-line in a local folder: `git clone https://gitlab.com/mygitlab/project_name.git`


## 3. Commit changes in the project

1. To add files to be committed: `git add filename` or `git add .` to add all the changes to the stage
2. To remove files from the stage: `rm --cached filename`
3. To commit changes: `git commit -m "commit message here"` 
4. To undo just the last commit, leaving all files intact: `git reset --soft HEAD^`
5. To undo the last commit and staged files: `git reset HEAD^`
6. To go back to the last commit by throwing away everything after that commit: `git reset --hard HEAD^`
7. To see all the commits done with details: `git log`
8. To see just the last n commits: `git log -n`
9. To see all the commits resumed in one line: `git log --oneline`
10. To see just the last n commits resumed in one line: `git log -n --oneline`
11. To see exactly what happened in one specific commit, copy it's rash number and type: `git show rashnumber`
12. To change the commit message of the last commit: `git commit --amend -m "new message here"`
13. To see the number of commits in your actual branch: `git rev-list --count HEAD `
14. To see the number of commits in a specific branch: `git rev-list --count <branch-name>`
15. To ignore items that you don't want to commit, like videos or compiled files for example, you can create a file named `.gitignore` and write on in the name of the files you don't want to track with git. If you want to ignore all .txt files in the same place of your .gitignore file for example, you can write in gitignore just `.txt` that all the .txt files of the same folder of your .gitignore file will be ignored.

## 4. Connecting with GitLab remote repository

1. To stablish a connection with a GitLab project repository: `git remote add origin URL` where URL is the https URL generated in the GitLab project.
2. The name origin is the nickname of the URL, to see witch nickname you gave to that URL, type: `git remote`
3. To see the full URL instead of it's nickname: `git remote -v`
4. To push commits to the remote project in GitLab: `git push origin master`
5. To pull commits from the remote project in GitLab: `git pull origin master`

## References

1. Curso da Udemy: Git e GitHub - Básico e Intermediário (https://www.udemy.com/course/git-e-github-basico-e-intermediario) 
2. GitLab (https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html)
3. Stack Overflow - https://stackoverflow.com/
