---
Author: André Jackson Ramos Simões
Advisor: Julien Chopin
First version date: June 2020
---

# Surface Evolver Scripts Introduction

As its name says, Surface Evolver (S.E.) is a powerful software that evolves surfaces from an initial state to a final state. The initial state of the surface is established by the user of S.E. through a script in a .fe file, and then the software uses algorithms based in the Physical Laws that rules the behavior of surfaces to move the surface until it reaches a final state, an equilibrium state. In this .fe scripts, there are a lot of things that can be done, and so, the goal of this tutorial is to offer an introduction of how S.E. scripts work and how to establish initial surfaces by writing those scripts.


## Evolving a cubic surface

One of the simpler examples of how S.E. works is the cube.fe example in the workshop of the creator of Surface Evolver Ken Brakke (Reference [1]), and to understand the basics of the .fe scripts it is perfect.



<img src = "img/cube.gif" width = "400">

​		 					 					**GIF 1** - Surface Evolver simulation with cube.fe file.



In GIF 1, it's possible to notice that in this simulation there was a cubic surface with a few points, and they move as the evolution process goes. The cube evolves like that because in this case there is only surface tension forces acting on the points of the surface. So, the equilibrium shape is the one that minimizes the area of the surface, with a constant volume V, and that results on a sphere. 

Focusing on the initial cube configuration, that is the surface established by the script of cube.fe, it is necessary to write a few things to establish the surface properly: vertices, edges, faces and the body of the surface. To illustrate how that is done, the cube.fe script follows:

```Surface Evolver
// cube.fe
// Evolver data for cube of prescribed volume.

vertices
1  0.0 0.0 0.0 
2  1.0 0.0 0.0
3  1.0 1.0 0.0
4  0.0 1.0 0.0
5  0.0 0.0 1.0
6  1.0 0.0 1.0
7  1.0 1.0 1.0
8  0.0 1.0 1.0

edges  /* given by endpoints and attribute */
1   1 2   
2   2 3   
3   3 4  
4   4 1 
5   5 6
6   6 7  
7   7 8 
8   8 5
9   1 5   
10  2 6  
11  3 7 
12  4 8

faces  /* given by oriented edge loop */
1   1 10 -5  -9
2   2 11 -6 -10
3   3 12 -7 -11
4   4  9 -8 -12
5   5  6  7   8
6  -4 -3 -2  -1

bodies  /* one body, defined by its oriented faces */
1   1 2 3 4 5 6  volume 1
```
### Vertices

By selecting the script cube.fe and editing it as a .txt file with a notepad for example, there is a section specially for vertices. They are defined by its coordinates related to a reference frame. In this case, the vertices are:

	vertices
	1  0.0 0.0 0.0 
	2  1.0 0.0 0.0
	3  1.0 1.0 0.0
	4  0.0 1.0 0.0
	5  0.0 0.0 1.0
	6  1.0 0.0 1.0
	7  1.0 1.0 1.0
	8  0.0 1.0 1.0

And these points would be in 3D space like this:

<img src = "img/vertices.png" width = "600">

​                                            **Figure 1** - Vertices of cube.fe script drawn in 3D space



One modification that could be done, for example, is to locate the vertices in a more symmetric position as in Figure 2:

<img src = "img/vsymmetric.png" width = "600">

​                              **Figure 2** - Repositioning vertices of cube.fe in a more symmetrical way.



The coordinates in this case would be written as:

	vertices
	1  -0.5  -0.5  0.0 
	2   0.5  -0.5  0.0  
	3   0.5	  0.5  0.0  
	4  -0.5   0.5  0.0  
	5  -0.5  -0.5  1.0
	6   0.5  -0.5  1.0
	7   0.5   0.5  1.0
	8  -0.5   0.5  1.0

### Edges

Edges are defined by two points, so an edge could be defined by joining the points 1 and 2 for example. But surface evolver deals with oriented edges, in a way that an edge defined by 1 2 is the opposite of the edge defined by 2 1, they are thought as vectors. In cube.fe they are defined this way:

	edges  /* given by endpoints and attribute */
	1   1 2   
	2   2 3   
	3   3 4  
	4   4 1 
	5   5 6
	6   6 7  
	7   7 8 
	8   8 5
	9   1 5   
	10  2 6  
	11  3 7 
	12  4 8

That would be graphically represented as figure 3 shows:

<img src = "img/edges.png" width = "600">

**Figure 3** - Drawing of the edges defined in cube.fe script. The numbers in the parenthesis indicates the number of its corresponding edge.



### Faces

As edges are defined by a set of points, oriented from point 1 to point 2, faces are defined by a set of edges, with also an orientation. To define a face in S.E. script it is necessary to establish the set of edges in a way that the circulation of these vectors points outward of the surface, with the right hand rule, like shown in Figure 4:

<img src = "img/surfacevector.png" width = "300">

​	        **Figure 4** - Surface vector $\vec{S}$ defined in a way that the circulation of the edges points outward.



With that in mind, the faces of the cube are defined as follows:

```surface evolver
faces  /* given by oriented edge loop */
1   1 10 -5  -9
2   2 11 -6 -10
3   3 12 -7 -11
4   4  9 -8 -12
5   5  6  7   8
6  -4 -3 -2  -1
```



<img src = "img/faces.png" width = "800">

**Figure 5** - Graphic view of the faces defined in the script of cube.fe. The faces are defined by a set of edges, and following the right hand rule, the area vector $\vec{S}$ always points outward of the body of the cube.



### Body and volume

As edges were defined by a set of points, and as faces were defined by a set of edges, bodies are defined by a set of faces. It is possible to see that in the script of cube.fe, the only one body, the body of the drop of water for example, or the body of the air bubble the cube could represent, is defined simply by combining the number of the faces:

```
bodies  /* one body, defined by its oriented faces */
1   1 2 3 4 5 6  volume 1
```

Next to the number of the faces, is the command `volume 1`, that sets the volume of the body to be equal to 1. Actually, it is a constraint, i.e., the surface will evolve always in a way that maintains its volume equal to 1. The initial cube was meant to have a volume equal to 1, as the constraint establishes, but it is possible to modify the constraint volume to 10 for example, and the vertices will evolve in a way that generates a bigger surface, of volume = 10, even if the initial configuration had a volume of 1. 




## References

[1] Surface Evolver Workshop, Kenneth A. Brakke: http://facstaff.susqu.edu/brakke/evolver/workshop/workshop.htm

[2] BRAKKE, Kenneth A. Surface Evolver Manual Version 2.70, 2013.