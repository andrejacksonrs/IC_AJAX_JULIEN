---
Author: André Jackson Ramos Simões
Advisor: Julien Chopin
Date: August, 2020.
---

# Surface Evolver Units System

In this document, we will talk about the measurement units in Surface Evolver. Actually, there are no units in Surface Evolver, it works dimensionless. Thus, we may have an initial difficulty to relate the numbers shown in Surface Evolver's simulations to real world measurements that are expressed with units such as the meter, second, kilogram, but in this document you will find a way to do that.

Especially, one goal that can be thought through doing simulations with Surface Evolver is to simulate the shape of a real drop with the volume of 1 cm³, for example, but how could we do that ? The goal of this tutorial is to discuss one way to do that kind of thing, relating Surface Evolver measurements, dimensionless, with dimensional quantities such as the meter, and kilogram for example.

## The capillary length normalization Hypothesis

In Capillarity and wetting phenomena, there is an important physical quantitie that appears all the time: The capillary length. It is an important characteristic quantitie of the system, such as the time constant in resistor capacitor circuits, and other physical systems. So, this length is such that all the lenghts of the system can be thought as a multiple of it, in a way we can think about normalizing the system lengths in relation to the capillary length.

Taking the drop simulation as an example, to establish a relation of the numbers in Surface Evolver and the real world measurements, a capillary length normalization can be thought.

The Capillary Length is a characteristic length of capillarity and wetting phenomena systems, that is defined as:
$$
Capillary \, Length = \kappa^{-1} = \sqrt{\frac{\gamma}{\rho g}}
$$
where:

$\gamma$ : Surface tension of the fluid-air interface

$\rho$ : Density of the fluid

$g$ : Gravitational constant



With dimensional analysis we can really see that this square root represents a length:

<img src = "img/SEunits1.png" >

**Figure 1** - Dimensional Analysis to show that the capillary length's dimension is really a length dimension. The brackets [ ] can be read as "The dimension of", such that [$\gamma$] means "The dimension of $\gamma$", for example.



With this length, measurements taken from a real water drop in an experiment can be normalized, for example suppose that the height of a drop was measured 5 mm. The capillary length in this setting is approximately 2,68 mm, as shown in Figure 2:

<img src = "img/SEunits2.png" >

**Figure 2** - Calculating the capillary length for a water drop. The values used were $\gamma$ = 0,072 N/m, $\rho$ = 1000 kg/m³, and $g$ = 10 m/s².



So, one way to think about the lengths shown in Surface Evolver exported data is to suppose it came from a capillary length normalization, and thus, to obtain the length unit desired, one way to proceed is just by multiplying the number shown in Surface Evolver. But remember, until now, this is only a Hypothesis, how could we guarantee that this procedure is valid ? The next topic is about that.


## The capillary length normalization as a consequence

In Surface Evolver, specially in the drop simulation scenario, we can define the values of, $g$, $\rho$, $\gamma$, and $V$ in the .fe scripts. These quantities relate to themselves somehow, and we can think about setting the values of V, and the lengths of the system, in terms of the previously defined $g$, $\rho$, $\gamma$.

If we set g = 10 in the Surface Evolver .fe script, for example, we can relate this number to our g = 10 m/s² , in a way that 10 Surface Evolver g units are equal to 10 m/s², or 1 Surface Evolver g unit (1 SEgu, abbreviated) is equal to 1 m/s².

Setting $\rho = 1$ to correspond to 1000 kg/m³, we can relate the Surface Evolver units to our daily units in a way that 1 SE$\rho$u = 1000 kg/m³.

Setting now $\gamma$ = 1, to correspond to 0,073 N/m = 0,073 J/m², which is the water's surface tension, we can relate 1 SE$\gamma$u = 0,073 N/m.

Dimensionally speaking, the quantities $g$, $\rho$, $\gamma$ are independent, like linear independent vectors, and thus we can combine them to derive any of the fundamental quantities inside their definition, such as mass, length and time. So, to relate the measurements in Surface Evolver to the real world measurements, we can combine these quantities and see what should be the Surface Evolver length unit, SElu abreviated:
$$
1 \, SElu = \sqrt{\left(\frac{1 SE \gamma u}{1SE \rho u \, . \,1SE g u}\right)}
$$
Because this is the only dimensional way to relate $g$, $\rho$, $\gamma$ to produce lengths. So, by substituting previous established values of the real world quantities into that equation, we can get that:
$$
1 \, SElu \approx 8,54 \, mm
$$
And with that, we can measure the height of the drop for example, and relate the measurement done in SElu to mm !

So, in fact, it seems that naturally there is a capillary length normalization we can think, because is the only dimensional way that lengths can be expressed in terms of $g$, $\rho$, $\gamma$ .

## Conclusion

Once we establish the values of  $g$, $\rho$,  and $\gamma$ in Surface Evolver, we can relate the quantities measure in Surface Evolver to the real world by making an equivalence of the numbers of $g$, $\rho$,  and $\gamma$ and certain values of  $g$, $\rho$,  and $\gamma$ taken as a reference. The conversion goes through a capillary length shape equation, and it indicates how we can think about the dimensionless numbers in Surface Evolver as they belong to a certain Surface Evolver unit system. This method was only thought and not put in really extensive practice, so maybe it must be refined to correct maybe practical or even theoretical mistakes. As the author of this thinking, i really say that humbly and happy to know if you find any mistakes and also maybe applications where it works, this document is definitely open to modifications and refinements.




## References

