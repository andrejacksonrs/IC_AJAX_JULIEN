---
Author: André Jackson Ramos Simões
Advisor: Julien Chopin
First version date: June 2020
---

# Surface Evolver Evolution Process Introduction

Surface Evolver is a software that works with user commands to evolve a surface until it reaches an equilibrium state. The goal of this brief tutorial is to present a way to do this evolution, thinking about the logic behind the evolution commands. This logic includes the use of the principle of minimum energy by the software, and the basic algorithm in Surface Evolver to do that, the gradient descent method.


## Overlook at how would be a Surface Evolver Evolution

To understand how to command evolutions in Surface Evolver, one simple script provided by Ken Brakke in his workshop [1] will be used: The cube.fe Example. In this example, a cubic surface representing the surface of a tiny water drop for example, evolves until it reaches its least energy shape, a spherical shape.



<img src = "img/cube.gif" width = "400">

​		 					 					**GIF 1** - Surface Evolver simulation with cube.fe file.



With GIF 1, it's possible to see how evolution goes in Surface Evolver: Initially there is a surface, and then the vertices move, the mesh is refined to generate more vertices and turn possible new movements to achieve the final equilibrium state of the surface.

## The Surface Evolver prompt window

To command the evolution as shown in GIF 1 for example, a command prompt is often used. With Surface Evolver installed on a computer, the file cube.fe can be opened with it, and what would pop up is a prompt window as shown in Figure 1:

<img src = "img/prompt.png" width = "600">

​                                         **Figure 1** - Surface Evolver command prompt running cube.fe



## Seeing the surface 

Typing `s` at the prompt will show the surface, as in Figure 2. The system has entered in the graphic mode (commands are now graphic commands, like commands to move the view of the surface. To evolve the surface it's necessary to quit this mode by typing `q`).

<img src = "img/prompt2.png" width = "600">

​                                               **Figure 2** - Surface Evolver graphic view of cube.fe

To see the surface from a perspective view, its possible to type `rrrrddddssss` to move the view 4 times to the right, 4 times downwards, and zoom out 4 times, respectively, or use the mouse's left button to control it more freely. More graphical commands can be found at Brakke's Workshop [1] and the Manual [2].

<img src = "img/prompt3.png" width = "600">

​                                     **Figure 3** - Surface Evolver graphic view of cube.fe in perspective



## Evolving the Surface

Now that the surface can be seen, it's time to evolve it, but before starting to evolve the surface of the cube, it's important to make sure the user is not on the graphic mode. So, if you entered the graphic mode by typing `s`, make sure you type `q` before starting to do evolutions commands, because the same letter can mean different commands depending on the mode surface evolver is.

To evolve this cubic surface as in GIF 1, there are mainly 2 commands written in the Surface Evolver command prompt:

`g` - Do one evolution step, i.e. one iteration, to move the vertices of the surface

`r` - Refine the vertices mesh, creating more vertices, and more triangles

After doing one single evolution command `g` in the prompt, this would be the result, as shown in Figure 4:

<img src = "img/1evo.png" width = "600">

​                    **Figure 4** - The shape of the surface after one single evolution, using the script cube.fe.

Taking a look at the prompt, after entering the `g` command, a list of values appears as:

`1. area:  5.13907252918614 energy:  5.13907252918614  scale: 0.185675`

- The number 1 represents the number of the iteration: This was the first iteration done, and thus the number 1 is there.
- The area number is actually the area of the surface after the iteration.
- The energy number is the energy of the surface after the iteration.
- The scale number is a factor that will be discussed in more detail later with the gradient descent method, but it can be understood now as a scalar, a number that multiplies the force vector in each point of the surface to properly move them.

To continue the evolution process, it's possible to do more iterations through the g command. But after a few evolutions, the cube will not move too much with the `g` commands, and the energy/area values will also be static. In this stage where the surface don't change too much after an evolution, it might be interesting to do a refinement through the command `r` . Opening the cube.fe and doing evolutions, after the third evolution the surface do not change a lot, so this would be a nice stage to make a refinement, and the result is shown in Figure 5: The mesh is refined, more points appear and new possibilities of movements exist now to move the surface to achieve a least energy configuration, that is the goal of the sofware. 

<img src = "img/1ref.png" width = "600">

​                   **Figure 5** - The shape of the surface after one single refinement, using the script cube.fe.

With that in mind, the process may go on doing evolutions and refinements until the equilibrium shape is achieved. Sometimes it's interesting to do a lot of iterations with just one command, and for that the `g` command can be complemented with a number after it. For example, `g 5` is a single command that do 5 evolutions, and in general it's possible to write `g x` where x is the number of iterations desired to happen.

The scale factor through this process of evolution may small up, and this is a thing that have to be avoided in the most of situations. With this scale factor very small, no matter the force on each point of the surface, the force will vanish because the scale factor multiplies it, and thus the surface will not move even if there were big forces to decrease the surface energy. In equilibrium, the forces are very small, if not equal to zero. So, the usually desired spot is one that the scale factor is normal/big, but despite of that, the forces are so small that the vertices move just a little with evolution commands.

## The `g` command 

### Surface Energy

As approached before, the main goal of evolving with Surface Evolver is to obtain an equilibrium shape of a certain surface. To do that, each evolution tries to minimize the energy of the surface, since the equilibrium state of a surface has a minimum energy.

The energy of a surface can depend on many variables, but mainly, it depends on the area of the surface, in a way that the surface energy in a lot of settings can be simply computed as:

$$
U = \gamma . A
$$

where:

$U$ = The energy of the surface

$\gamma$ = Surface Tension of the surface

$A$ = Area of the surface



In this important case, the energy $U$ of the surface is proportional to its area, in a way that minimizing the energy of the surface is the same thing as minimizing the area of the surface. In more complex cases, the energy may depend on gravity, and other forms of energy, but to understand the g command, this simple case is enough. Also, one simple surface evolver script may help in the understanding of how Surface Evolver minimizes the surface energy, the Pyramid.fe script, that can be found in the following link: 

Pyramid.fe script:

[https://gitlab.com/andrejacksonrs/IC_AJAX_JULIEN/-/blob/master/Tutorials/Surface%20Evolver/pyramid.fe](https://gitlab.com/andrejacksonrs/IC_AJAX_JULIEN/-/blob/master/Tutorials/Surface Evolver/pyramid.fe)

<img src = "img/pyramids.png" width = "395"> <img src = "img/pyramidfe.png" width = "395">

**Figure 6** - Pyramid.fe Script runned on Surface Evolver at the left, and sketch about its points at the right. In this example, all the points on the base of the pyramid are fixed, and only the top vertice $P_5 = (x,y,z)$ can move.

So, to minimize the energy of the surface, Surface Evolver first calculates its actual energy by calculating the area of the surface. To do that by hand, the vectorial product can be used as shown in Figure 7:

<img src = "img/A1.png" width = "500">

**Figure 7** - How to calculate the area of  a triangle of the surface using the cross product. The module of the cross product is exactly the area of the parallelogram formed by the two vectors of the product. So, the area of the triangle delimited by them is half of the area of the parallelogram, and thus, half of the module of the cross product.

Imagine now a surface full of triangles, this calculations would be tedious and laborious, and because of that, the computer is used to speed up the calculation of the total area of the surface. 

### Gradient Descent Method

Despite of that, in this simple example, these calculations can be done without much work, but still a lot. Finally, an expression for the area as a function of the $P_5$ position comes as (this is an approximation for x,y and z much smaller than 1):
$$
A = A(x,y,z) = A(P_5) = x^2 + y^2 + 2z^2 + 4
$$
And with that, here comes the gradient. The gradient $\nabla A$ is a vector that has a beautiful and very important property: In its direction, the Function A increases the most, and thus, in the opposite direction of it, the function A decreases the most. So, the point $P_5$ is moved in the direction opposite to the area gradient, to decrease the surface energy. That, done to all of the vertices of the surface, would be an iteration step.
$$
\nabla A = \left(\frac{\partial A}{\partial x},\frac{\partial A}{\partial y},\frac{\partial A}{\partial z}\right) = (2x,2y,4z) \therefore -\nabla A = (-2x,-2y,-4z)
$$
Also, because the energy of the surface is proportional to its area, comes that: 
$$
U = \gamma . A = \gamma . (x^2 + y^2 + 2z^2 + 4) \therefore \nabla U = \nabla (\gamma . A) = \gamma . \nabla A = \gamma . (2x,2y,4z) = (2 \gamma x, 2\gamma y, 4 \gamma z)
$$
And thus, because $\gamma$ is constant, and positive, the direction of minimizing area is the same of minimizing energy. The opposite of the energy gradient is actually the force acting on that vertice.

### Conclusion

This may be a very helpful way to think about the g command: 

Surface Evolver does the job of calculating the area of every triangle. Fixing all of the points but one, it calculates the energy of the surface depending on the position of that one, its gradient, and then move this vertice on the opposite direction of the gradient to decrease the total energy of the surface. With that the point is displaced by an amount like $(scale . -\nabla U) = scale.F$. Doing that again and again, the force $F$ should be next to zero, i.e., the surface reached an equilibrium state, and that is the gradient descent method, where a function descent to a minimum value through steps done in the opposite direction of the gradient.

It is important though to emphasize that this is a simplified idea of what would be going while doing the g command, actually, it is more complex and has a lot of steps that were not discussed here but can be seen in the workshop [1] in the following link: 

The g command detailed steps:

[http://facstaff.susqu.edu/brakke/evolver/workshop/doc/iterate.htm#gradient%20descent%20iteration](http://facstaff.susqu.edu/brakke/evolver/workshop/doc/iterate.htm#gradient descent iteration)




## References

[1] BRAKKE, Kenneth A. - Surface Evolver Workshop: http://facstaff.susqu.edu/brakke/evolver/workshop/workshop.htm

[2] BRAKKE, Kenneth A. - Surface Evolver Manual Version 2.70, 2013.

[3] SHEWCHUK, J. R. - An Introduction to the Conjugate Gradient Method Without the Agonizing Pain. School of Computer Science Carnegie Mellon University Pittsburgh: 1994.

[4] REFSNAES, R. H. - A brief introduction to the Conjugate Gadient method. Fall 2009.

[5] SIMÕES, A. J. R. - Surface Evolver scripts introduction. 2020.