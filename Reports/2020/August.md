# Ajax's scientific initiation reports

## August 2020

### 1. 08/01/2020

Activities:

a) Work on the website and readme

Problems:

Results:

a) Website and Readme final first edition done

### 2. 08/02/2020

Activities:

a) Update website adding credits to the pictures used 

b) Work on Final Report

Problems:

Results:

a) The author of The bird beautiful picture authorized me to use it :D

b) also, checked a video about public domain pictures

https://www.youtube.com/watch?v=z517_OdxHbk

c) Credits on the legend written

### 3. 08/03/2020

Activities:

a) Work on the final report

Problems:

Results:

a) Finished final report with images. The only thing left is the signature of prof. Julien

### 4. 08/04/2020

Activities:

a) Finish and Submit final report  to SISBIC

b) Meet with prof. Julien 

Problems:

Results:

a) Add to the project a conversion of surface evolver's units to SI units for example

b) See opensource resources: 

- Sozi for SVG presentations
- mkdocs for websites
- textext for latex in inkscape
- pdfsam to mix pdfs and other pdf stuff

### 5. 08/07/2020

Activities:

a) Work on "calibrating" surface evolver

Problems:

Results:

a) Demonstration using linear algebra ideas that $\gamma , g , \rho$ are independent in a way that we can define mass, time and length with them, and thus volume, area, and all the physical quantities derived from it.

### 6. 08/10/2020

Activities:

a) Edit readme as the same as the website

b) Make website with mkdocs

Problems:

Results:

a) Readme edited, but gitlab markdown doesn't read it properly as typora do, the youtube videos for example

b)  Copy paste mkdocs to project folder, it worked, i just have to adjust the file adresses it seems

### 7. 08/11/2020

Activities:

a) work on the website

Problems:

a) how to try website before pushing up to gitlab

Results:

a) Edit theme and move some images locations, also change readme

b) submit issue to prof. julien about how to try website page before pushing up to gitlab

### 8. 08/13/2020

Activities:

a) learn mkdocs stuff

Problems:

Results:

a) Installed mkdocs in anaconda prompt by just typing pip install mkdocs

b) installed mkdocs bookstrap to able themes like cerulean (i loved cerulean :3)

### 9. 08/14/2020

Activities:

a) Work on website page

Problems:

Results:

a) Edited the website, putting links on references, to the images and other adresses that were there without links. Fixed youtube video sizes to 500 x 281, perfect size. A new index of the website was put, very beautiful.

### 10. 08/17/2020

Activities:

a) Work on website

Problems:

Results:

a) Learned how to put new pages in the website, through .yml file.

b) Correct superficial -> simplified in surface evolver evolution tutorial

### 11. 08/25/2020

Activities:

a) Organize git gitlab workshop

Problems:

Results:

### 12. 08/31/2020

Activities:

a) Write Surface Evolver Tutorial about units

Problems:

Results:

### x. 08/xx/2020

Activities:

Problems:

Results:

### x. 08/xx/2020

Activities:

Problems:

Results:

### x. 08/xx/2020

Activities:

Problems:

Results:

### x. 08/xx/2020

Activities:

Problems:

Results:

### x. 08/xx/2020

Activities:

Problems:

Results:

### x. 08/xx/2020

Activities:

Problems:

Results:

### x. 08/xx/2020

Activities:

Problems:

Results:

### x. 08/xx/2020

Activities:

Problems:

Results:

### x. 08/xx/2020

Activities:

Problems:

Results: