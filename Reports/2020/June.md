# Ajax's scientific initiation reports

## June, 2020

### 1. 06/01/2020

Activities:

a) Organize GitLab scientific initiation project in general, learn how to use gitignore

Problems:

Results:

a) Organized, files from ajaxcapillar transfered to IC_AJAX_JULIEN project.



### 2. 06/02/2020

Activities:

a) Study volume effects over the shape of drops without and with gravity, using surface evolver and python

Problems:

a) Small volumes such as 0.01 and 0.02 in surface Evolver script went crazy with a little peak just in the middle. Without this point the shape of the drop seemed to be a very spherical one

b) GitLab seems to be incompatible with Typora in latex syntax

Results:

a) start a script to measure the effects of volume under the shape of drops



### 3. 06/03/2020

Activities:

a) keep working at the SE script to measure volume effects

Problems:

Results:

a) Established the volume work and how it will be done with SE and python: We will analise a drop configuration in g =10 and $\theta_E = \frac{\pi}{2}$ , varying volume from 0.05 to 5. 

b) very large volumes, above 5, and very small, below 0.05 may generate singularities in the simulation and thus they may not fit the experiment study.

### 4. 06/04/2020

Activities:

a) Process data from surface evolver with python to generate graphs of the shape of drops varying the volume of the drop

Problems:

Results:

a) figure of the volume study done

b) study the shapes and the thickness of drops, added a picture of a large drop in side with a small drop 

### 5. 06/05/2020

Activities:

a) Enhance the drop volume study figure as prof. Julien recommended

Problems:

a) Cannot simulate small volumes, volumes below 0.05, because the drop reaches a surface with a point in it's top

b) with g = 0, the maximum volume possible to simulate seems to be v = 20

Results:

a) To solve the problems, the rewrite of the size of the initial cube seemed to work !

b) Rewrite drops vertices in a symetric way to be easier to change the size of the initial cube that will evolve to a drop

### 6. 06/08/2020

Activities:

a) Simulate volumes from 0.001 to 10 as prof. Julien recommended, using the new script with vertices written symmetric to the origin of the coordinate system.

b) Relearn python processing data skills to make the script able to read the data in one single loop (I  already knew that but when my hd crashed i lost the script that contained the algorithm i made to do it)

Problems:

Results:

a) simulation of small and big volumes of drops are now possible by changing the size of the initial cube

b) Wonderful script made in python to process data from SE in a great loop, very much optimized

c) New figure done with new graphs, including semilog one just as prof. Julien recommended

### 7. 06/10/2020

Activities:

- [x] a) Put figure in drops document and explain the phenomena qualitatively
- [x] b) Find an explanation for the data in the literature
- [x] c) Work on a description of the phenomena, writing in drops.md

Problems:

- [x] I cannot simulate great volumes
  - Solved by decreasing the height of the initial cube, transforming it in a parallelepiped . It was going straight down with gradient descent method, in a way that convergence was not able to be reached out

Results:

### 8. 06/12/2020

Activities:

- [ ] a) Find a way to simulate volumes from 10 to 1000 to make the graph more interesting in relation to the theory prediction of the thicknes the drop should have for large volumes (it's not very easy, i'll focus on organizing things and maybe ask prof. Julien if it would be necessary to keep working on this)
- [ ] b) Organize drops.md document with all studies and explanation of surface evolver to the reader

Problems:

Results:

### 9. 06/15/2020

Activities:

- [x] a) Finish organization of drops.md document with all 3 studies (contact angle, gravity and volume)
- [x] b) Try to simulate volumes from 10 to 1000 to complete the graph of height vs volume
- [ ] c) make reference to SE files and python scripts in the drops.md document
- [x] d) Ask prof. Julien about starting the heterogeneity problem
- [x] e) Redo all the graphs with the python and SE scripts to make sure the results are reproducible
  - This was not very easy, i tried to redo the CA analysis but there were errors i don't understand, i think i'll wait to go back to ufba and see the script and all the files to my computer.
  - Gravity graphs and volume are all ok, just the C.A is left

Problems:

a) Trying to simulate volume = 1000:

- From a cube of side = 10 the top vertex go under the plane bugging the simulation
- increasing the size of the base area from A = 100 to A = 20.20 = 400, the top vertex still go under the plane bugging the simulation, but the distance from the plane is smaller
- Trying to increase more and more the base area, the peak still stay under the plane after one evolution, tested base area = 15² and 20² seeing that.
- Now trying to increase the height of the initial cube from 10.0 to 9.0, the peak remains, h = 5.0, h = 4.0 too, but h = 3.0 no problem with that peak under the plane, but still i found difficult to reach the final shape by evolutions, it needs a lot of evolutions to find the circular shape of the drop.
- h = 3.0 but Abase = 30.30 went with a little peak in the first evolution but at the next there was not bug, the peak returned, but still is very difficult to reach without thousands off evolutions the final shape circular. it seems that if the little peak under the plane is small, it can come back at the next evolution. h = 3.0 and Abase = 40² got the same problem.

b) The script of contact angle variation is not working, i tried to fix it but without success. I will rewrite it with an optimized version such as the volume one. Also, modify the .fe script to center the drop at the origin.

Results: 

a) Finished the first complete version of drops.md, and send to prof. julien.

### 10. 06/17/2020

Activities:

a) Finish git/gitlab tutorials

b) Stablish with prof. Julien future activities:

- Edit Readme

- Simulate 2 planes with drop

- Explanation of SE script .md file
- Clean files by renaming important images, excluding non important files, and renaming folders in a very easy to identify way and easy to localize in scripts like python ones 

Problems:

Results:

### 11. 06/18/2020

Activities:

a) Edit Readme

- [x] Create svg image of 3 drops with different contact angles
- [ ] Write an introduction to the project

b) Try to simulate 2 planes and a drop

Problems:

Results:

a) 2 images edited in inkscape to represent drops (drops photos and drawings)

b) Preliminar result of simulating heterogeneity over the drop

### 12. 06/20/2020

Activities:

a) Continue to writing Readme

- [x] Make gif of surface evolver evolution of drop and meniscus

b) Continue to work on simulation of heterogeneity

- [x] Try to make an upside down version of the regular drop by making a z=1 constraint

Problems:

Results:

a) Learned how to make gifs with GIMP, and then made a drop gif. 

b) Just changing the vertices and edges of contraint are not working, there is something i'm missing. Maybe the script free free_bdry.fe may help as a reference.

c) Saw that in the SE documentation there's something about postscript pics or something like that, that may be useful to make better looking animations: http://facstaff.susqu.edu/brakke/evolver/html/graphics.htm#screen-graphics

### 13. 06/22/2020

Activities:

a) Try to make upside down simulation of drop 

b) Try to learn how to export publication like pictures from surface evolver through this link:  http://facstaff.susqu.edu/brakke/evolver/html/graphics.htm#screen-graphics

Problems:

a) Just reflexing vertexes is not working to simulate the drop upside down. One hypothesis to justify it is that the faces are no longer positive oriented.

Results:

a) We can simulate a drop without the plane scripted in mound.fe, we can simulate just the drop like this:

![image-20200622112007472](C:\Users\Andre\AppData\Roaming\Typora\typora-user-images\image-20200622112007472.png)

b) Drawing of the cube vertices and edges in mypaint open source software:

![image-20200622123612965](C:\Users\Andre\AppData\Roaming\Typora\typora-user-images\image-20200622123612965.png)



c) Doing a reorientation on the upside down cube to turn the faces positive oriented, it was possible to simulate a drop upside down, but that may be a sign that the initial idea of simulating a drop spreading over two planes cannot be done, because it should be necessary two orientations for the facets.

![image-20200622182020870](C:\Users\Andre\AppData\Roaming\Typora\typora-user-images\image-20200622182020870.png)

![image-20200622182313263](C:\Users\Andre\AppData\Roaming\Typora\typora-user-images\image-20200622182313263.png)

d) We can generate postscript publishing type image files with surface evolver with the command P 3 in the prompt:

![image-20200622192419059](C:\Users\Andre\AppData\Roaming\Typora\typora-user-images\image-20200622192419059.png)



### 14. 06/23/2020

Activities:

- [x] a) Try to do upside down simulation with z=1 in spite of z=0 and see if it works with correct orientation of facets

  - [x] did not work in principle, i'll try to just elevate every vertice to z=1 and see what happens, because if there is something wrong with doing z=1 it should not work too.

    The problem was on putting z=1, maybe by doing that the energy must be changed from PLANET*X to something different. I need to find out what is e1, e2 and e3.

  b) Make SE tutorial with help of drop and cube into sphere examples


Problems:

Results:

a) Density inclusion makes the drop wants to fall.

### 15. 06/24/2020

Activities:

a) Make SE tutorial explaining how the cube into sphere simulation works

Problems:

Results:

a) Found an example script in brakke's workshop called moundG.fe that could help in the plot of the second plane

b) Introduced vertices and gif to the se tutorial

### 16. 06/25/2020

Activities:

a) Continue to try to simulate the drop spreading over a plane of z = 1

- If you want to do commands in the script, it seems that the command **read** is necessary before the evolution/exhibition commands you would do in the prompt
- Evolving a sphere (all 6 faces) with the constraint, z=0 or z=1, was possible, with no problems.
- Removing the down face of the cube and setting z = 0 constraint, ok as expected. But removing the up face and setting z = 1 went to the same problem: after one evolution the up vertices shrink despite of spread as happened with z = 0.
- I will try to make the up vertices fixed, as in the free bdry example.

Problems:

Results:

### 17. 06/26/2020

Activities:

a) Continue writing S.E. Tutorial

Problems:

Results:

a) add positive oriented surface img to the tutorial

### 18. 06/29/2020

Activities:

a) keep writing surface evolver tutorial

- Enhance figures precisely
- write faces/body/volume section of the tutorial

b) Try to simulate the heterogen cubic particle

Problems:

Results:

a) Surface evolver scripts introduction finished



