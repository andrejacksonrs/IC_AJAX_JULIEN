# Ajax's scientific initiation reports

## July 2020

### 1. 07/01/2020

Activities 

a) Edit drops study.md with drop photos to describe phenomena before drawings

Problems:

Results:

a) Revised drops study.md file and edited it

### 2. 07/02/2020

Activities 

- [x] a) Edit Readme.md putting links of the works/tutorials and introducing things
- [x] b) meet with prof. Julien 


Problems:

Results:

a) Make tutorials about evolutions in surface evolver

b) Organize readme.md with nice images

c) put python tutorials organized easy to find like import data and fit

d) work with freebdry adapted .fe to generate the profile of the drop with an heterogeneity

### 3. 07/06/2020

Activities 

a)  Try to simulate profiles of drop with heterogeneity with freebdryadapted.fe

Problems:

Results:

a)                                               ![image-20200706182532467](C:\Users\Andre\AppData\Roaming\Typora\typora-user-images\image-20200706182532467.png)

**Figure 1** - Preliminar result of plotting - getting reproducible is the next step, i.e., put a number of commands in the script to get this shape, and plot the x negative part too.

b) `shift tab` = remove indentation of blocks of code

c)                                             ![image-20200706201411380](C:\Users\Andre\AppData\Roaming\Typora\typora-user-images\image-20200706201411380.png)

**Figure 2** - Nice reproducible result of the shape of a drop in contact with an heterogeneity cube bottom.

### 4. 07/08/2020

Activities:

a) Prepare list of essential python scripts to do research and put them in the tutorial section

b) Put the tutorials done today in the .readme with a nice picture attracting people

Problems:

Results:

a) Tutorials of how to write and import data to plot and export them, 2D and 3D written and pushed to gitlab.

b) Linear and Nonlinear fit tutorial done

### 5. 07/09/2020

Activities:

a) Submit issue about how to make numerical variational calculus to measure force from surface evolver simulations

b) Put list of python scripts developed yesterday in the readme.md

c) Start Developing Surface Evolver tutorial about how to evolve surfaces

d) Simulate different shapes with z constraints slightly different as prof. Julien advised me

Problems:

Results:

a) Activity a done

b) Activity b done

c) Write part about the graphic interface of surface evolver into evolution tutorial .md file

d) ![image-20200709222746987](C:\Users\Andre\AppData\Roaming\Typora\typora-user-images\image-20200709222746987.png)

Shapes requested simulated, as put in activity d. Force measured: F = 0.7

### 6. 07/13/2020

Activities:

a) Check in Brakke's manual/workshop what exactly is the value of energy displayed in the prompt

- Did not find

b) Keep writing surface evolver's evolution tutorial

Problems:

Results:

a) Part of evolution commands done in the tutorial

### 7. 07/15/2020

Activities:

a) Keep editing readme.md file

Problems:

a) Video not appearing in gitlab's markdown but appearing in typora

Results:

a) added .mp4 video of capillary rise to the readme file.

b) solved the problem with this page of gitlab flavored markdown: https://docs.gitlab.com/ee/user/markdown.html 

it says that we should just write `![title of the video here](place of the file here)` - but i downloaded the gitlab theme for typora and still i could not write the video inside the readme.md document as this page told. but by doing that inside gitlab website it really worked. I will for instance do everything here on typora and at the end convert it to gitlab markdown syntaxes.

c) added new figure of drop real vs simulated in drops study readme part

d) added to readme a figure of contact line study

### 8. 07/20/2020

Activities: 

a) Keep developing surface evolver tutorial about evolutions

b) Try to recover files from FESC's pc and Julien's notebook

c) Keep editing readme.md

d) Start final report of the scientific initiation project

Problems:

Results:

### 9. 07/21/2020

Activities: 

a) Edit Readme

b) Think about the gradient descent method to present it in the evolution tutorial for surface evolver

Problems:

Results:

a) Maybe a simple example of computing $U = U(x,y,z)$ with help of a tetrahedron, show that the displacement of one vertex is according with the gradient

### 10. 07/22/2020

Activities: 

a) See with Robson, Prof. Roberto if everything is ok in FESC for me to go there and collect files from the computer i used to use.

b) Try to finish evolution tutorial of Surface Evolver

Problems:

Results:

a) started to talk about energy in the evolution process tutorial

### 11. 07/23/2020

Activities: 

a) Meet with prof. Julien

Problems:

Results:

a) Do simple example in the SE tutorial calculating the energy as a function of the position of just one vertice, and then realize that it may be the first step to the heterogeneity problem of the magnet over the drop.

### 12. 07/24/2020

Activities: 

a) Go to ufba save files from FESC pc into my pendrive

Problems:

Results:

a) files saved successfully

### 13. 07/27/2020

Activities: 

a) Organize last week of official activities to finish scientific initiation

b) Finish surface evolver evolution tutorial 

Problems:

Results:

a) Area gradient, and thus energy gradient, analytically calculated for the pyramid.fe example, everything on the medibang file.

### 14. 07/28/2020

Activities: 

a) Keep working on the gradient example

Problems:

Results:

a) Calculated area function with approximations

b) Exemplified how would occur a change in position of a point

### 15. 07/29/2020

Activities: 

a) Work on the SE evolution tutorial

Problems:

Results:

a) Organized ideas to put on tutorial, and downloaded video of drop with magnetic heterogeneities 

### 16. 07/30/2020

Activities: 

a) Edit capillary rise experiment video and put it on readme.md

b) Finish evolution process surface evolver tutorial

Problems:

Results:

a) Activity a done !

b) Activity b done !

### 17. 07/31/2020

Activities: 

a) Report heterogeneity effect result in drops study.md

b) Started to see the problems in theory of contact line document to fix it 

Problems:

Results:

a) Heterogeneity study included on drops.md file

