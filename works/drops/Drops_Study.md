---
author: André Jackson Ramos Simoes
advisor: Julien Chopin
date: 2020
---

# Studying the shape of drops with Surface Evolver

## Abstract

When a water drop sits in a surface, it can assume different shapes depending on the substrate it is on. The shape of a drop can depend on a lot of parameters of the system, such as the contact angle that the drop presents, it's volume, and the gravity acting under it. So, to attack the problem of understanding how the shape of a drop changes in different settings, this document presents a way to do that by using the simulation software Surface Evolver. Here will be presented results of how Surface Evolver can be used to see the effects of the contact angle, gravity, the volume of a drop, and single heterogeneities on the shape of the drops. 



## Contact Angle effects under the shape of drops

### Phenomena Description

To define what is called the contact angle, let's notice that when a drop sits in a certain substrate, its shape can be different, as in Figure 1:


|          <img src = "img/drops.png" width = "800">           |
| :----------------------------------------------------------: |
| **Figure 1**- Drops of water on different types of surfaces: (a) - A drop of water on a yellow eraser. (b) - A drop of water on the back of a lock shaped speaker, a non-wetting surface. (c) - a drop of water on a cleaning cloth. |

|           <img src = "img/CAf.png" width = "500">            |
| :----------------------------------------------------------: |
| **Figure 2** - The contact angle is the angle inside the fluid that is between the gray tangent line and the surface of the substrate. That tangent line is the line that is tangent to the surface of the fluid and passes through the point that divides the 3 media involved: Air, Fluid and Substrate. |

| <img src = "img/allangles.png" width = "800">                |
| :----------------------------------------------------------- |
| **Figure 3** - The influence of the contact angle under the shape of a drop, the greater the contact angle, the higher the drop is and more next to a perfect sphere. This angle will be represented in this document as $\theta_E$. From the left to the right, there are configurations of: $\theta_E < 90º$, $\theta_E = 90º$ and  $\theta_E > 90º$. |

Incredibly, this angle is a property of the materials involved: The fluid, the substrate and the gas around the drop. With this, in principle, it would not matter if the drop has a large volume, or the gravitational constant in the place changes, the angle would be the same. But depending on the material a drop of water sits, for example, it's contact angle would change, and this affects the shape of the drop as can be seen in Figure 3. To measure these effects on the shapes, Surface Evolver is a simulation tool that can simulate drops only varying it's contact angle, and doing that for different angle we can see how the profile of the drop changes in Figure 4.

| <img src = "img/AF1.png" width = "1000">                     |
| ------------------------------------------------------------ |
| **Figure 4** - (a) Drop of water in a hydrophobic frying pan. (b) Drop simulation with Surface Evolver. (c) Different profiles of drops (first quadrant) depending on it's contact angle. The plot was done with data from Surface Evolver in a zero gravity and volume = 1 setting. (d) Relation between the radius of drops (measured from Surface Evolver data as in (c)) and it's contact angle . |
| **OBS**: All the graphs were done using data from Surface Evolver simulations, and thus the axis and the data in the table do not have units because Surface Evolver work dimensionless. |

In Figure 4 - c it's possible to see that increasing the contact angle the drop becomes higher. Also, the shape turns to be more next to a sphere, and it's even possible to measure the radius of the sphere that would contain the drop as in Figure 5. 

| <img src = "img/R90.png" width = "380"> <img src = "img/R.png" width = "380"> |
| :----------------------------------------------------------: |
| **Figure 5** - Measuring the Radius of the sphere that contains a drop. |

### Why do this happen ?

One first question we could ask by seeing these results would be: Why is the shape like a sphere ? Well, the settings used in Surface Evolver to simulate the drops were such that there was no gravity acting, and in this setting the only forces acting under the drop are the surface tension forces. Surface tension is a property of the surfaces of materials, specially fluids, that is the root of forces that tends to minimize the area of the surface, and minimize its energy. So, a determined volume of a drop changes it's shape to reach the least area possible, and that is the shape of a sphere.

Another thing to be explained is the Figure 4 - d. Knowing that the drops all have the same volume, this shape is geometrically predicted by the following equation:
$$
R(\theta_E) = a.\left\{ \left(\frac{3}{\pi}\right).[2+cos(\theta_E)]^{-1} . [1 - cos(\theta_E)]^{-2}  \right\}^{b}
$$
This very big equation came from the idea that the volume of the drop is a spherical cap, and the parameters a and b should be adjusted to the data to precisely fit the experimental points. The parameter a should be next to the length of the cube of the same volume of the drop, and b approximately -0.33, representing a cubic root. So, we can explain that when a drop have a bigger contact angle, with a fixed volume V it tends to be higher, and to compensate that height in a way to preserve its volume, the drop squishes it's radius. Thus, the greater the contact angle, the higher the height of the drop, and the smaller the radius.



## Gravity effects under the shape of drops

### Phenomena Description

Imagine a drop of water sitting on the ground here on the planet earth where the gravity constant is approximately 9.8 m/s². Have you ever wondered how would the shape of that drop change if the gravity constant of earth were bigger or smaller ? Or even how would be the shape of a drop in mars different from here ?

These questions may not be answered easily by experiments because it is not easy to change the gravitational field acting under the drop. But, with Surface Evolver it is possible to simulate how the drop would be by varying the gravity constant. By doing that, what we see is that the greater the gravitational constant the more squished the drop stays, squished in such a way that the height (or thickness) of the drop decreases more and more as we can see in figure 6 and 7.

|         <img src = "gravity/Ege.svg" width = "1000">         |
| :----------------------------------------------------------: |
| **Figure 6** - Different shapes of drops simulated with Surface Evolver. A drop with a fixed volume v =1 and contact angle $\theta_E = 60º$ changes it shape depending on the gravitational field is acting under it. As the gravitational field increases the gravity squishes more the drop, turning it more plane. |

| <img src = "gravity/EExg.svg" width = "450"> <img src = "img/e.png" width = "300"> |
| :----------------------------------------------------------: |
| **Figure 7** -  In the same settings as exposed in Figure 6, it's possible to see that indeed the gravity effect under the shape of the drop is to squish it, decreasing it's thickness e. |

### Why do this happen ?

The gravitational field that acts under the mass of a drop is a vertical downward field that generates a vertical downward force, i.e. the weight, in each particle that compose the fluid. The weight, therefore, wants the drop to be as flat as it can. Also, the surface tension is acting under the drop, in a way that it wants to turn the drop into a sphere, opposing the gravity's intention against that With that in mind, the gravitational field naturally is a parameter that influences on how high the drop would be, because the greater the downward force, the hard would be to the surface tension forces to compensate that height.




## Volume effects under the shape of drops 

### Phenomena description

When a single tiny drop sits on a surface, it tends to have a spherical shape, and the less the volume of this drop, the more spherical it is. Large volumes of water can form drops that usually have a different shape that is no longer spherical. This shape is approximately plane as the volume gets larger, although the surface of the drop remains curved near its boundary. 

| <img src = "img/vfigure.svg" width = "1000" >                |
| ------------------------------------------------------------ |
| **Figure 8** - (a) A large volume drop and a small one by it's side. The small drop is spherical, while the larger drop more plane in the middle. (b)  The effects of changing only the volume of a drop: it's height (or thickness) tends to grow with volume, until it stabilizes for large volumes. (c) Drop profiles of different volume drops under the same settings of gravity (g = 9.8) and contact angle ($\theta_E = \frac{\pi}{2}$). |
| **OBS**: All the graphs were done using data from Surface Evolver simulations, and thus the axis and the data in the table do not have units because Surface Evolver work dimensionless. |

### Why do this happen ?

It is possible to understand this phenomena by thinking about the forces acting under the drop. There are mainly 2 forces acting on the drop, it's weight due to gravity, and the surface tension force acting on the contact line of the drop.

For tiny drops, the effects o gravity are almost none, because these drops have small mass. So, for small drops, the force that rules its shape are surface tension forces that act in a way to make the surface more spherical, minimizing its energy.

When the volume of the drop is greater, then the effects of gravity are not negligible, because the mass is greater now and so is the weight. The gravity now tries to squish the drop, while the surface tension tries to make the surface spherical, and together they result in that intermediate shape between a plane and a spherical shape. 

As the volume of the drop increases, the gravity increases too, and then the surface tension effects vanish, specially because surface tension forces are so that they tend to appear only in small scales. Thus, the drop tries to reach a planer surface, as we can see in the Figure 8 - c.

### Predicting the Height of the drop

For large volumes, the shape of the drops are pretty much plane, and in this situation it is possible to predict the height of the drop (or the thickness e), based in other important physical quantities related to the drop. The following equation do that:
$$
e = 2\kappa ^{-1}sin\bigg(\frac{\theta_E}{2}\bigg)
$$
where

- e : thickness of the drop, the height of the highest point of the drop
- $\kappa^{-1} := \sqrt\frac{\gamma}{\rho g}$ : The capillary length
  - $\gamma$ : Surface tension of the interface drop-air
  - $\rho$ : Density of the drop
  - $g$ : gravity

- $\theta_E$ : The contact angle of the drop

  

In the experiment done with Surface Evolver to study the effects of volume under the shape of the drop (Creating Figure 8), the setting established was:

- $ g = 9.8$
- $ \gamma = 1$
- $\rho = 1$

And with that setting, it's possible to predict the thickness that the drop should have for large volumes. Calculating that, $e = 0.45$ is obtained, witch is a value that is next of what can be seen in Figure 8 - b. The value on this graph is actually a little higher than the 0.45 predicted, showing that to reach the thickness predicted it would be necessary to simulate even larger volume drops, but it is quite next to it. More exactly speaking, the thickness of the larger drop simulated was 0.48 that differs to 0.45 by 6%.



## A single heterogeneity effect under the shape of drops

### Phenomena description

When the surface of a fluid is pinched, or simply deformed by an external agent, it behaves in a way that a new curvature appears on it. Specifically, when small heterogeneities are in contact with water drops it produces a curvature that is interesting, forcing the heterogeneity to stay inside the drop, as shown in Figure 9.

<img src = "heterogen/dropparticle.png" width = "300" >

**Figure 9** - A single magnetic particle at the surface of a drop forced by a magnetic field to come out of the yellow colored water drop.

### Why do this happen ?

When the heterogeneity tries to escape the drop by the magnetic force acting on it, new interfaces are created, forming a contact line as shown in Figure 10 by the grey dotted line.

<img src = "img/dropzoomhetero.png" width = "800" >

**Figure 10** - Forces acting on the contact line, i.e., the line that divides the three media: The heterogen particle, the fluid and the air. Those forces acts tangent to the surface on each element $dl$ of the contact line with module equals to $ dF = \gamma . dl$ , where $\gamma$ is the surface tension of the fluid.

Once this contact line is formed, the surface wants to minimize its energy, and thus it tries to minimize its area by pulling the contact line, and thus pulling the heterogeneity too.



### Measuring the Force on the heterogeneity with Surface Evolver

When a surface is evolved in Surface Evolver, its shape has an energy associated with it, and it is calculated by the software and exhibited in each iteration. So, the idea to measure the force acting on the heterogeneity is to simplify it, as a cube geometry, and then measure the energies associated with a tiny displacement of it, and from that measure the force as the derivative of the energy in respect to this tiny displacement. 

<img src = "heterogen/hetero.png" width = "800" >

**Figure 11** - Different profiles of a large drop in contact with a single cube heterogeneity. The energy is calculated by Surface Evolver and shown in its prompt window.

To simulate the drops as shown in Figure 11, a large drop was cosidered, and the low base of the heterogeneity cube considered made of fixed vertices, in a way that the drop would hug it forcing as in Figure 9. The difference of z values was 0.1 and the energy difference 0.07, so the force would be in module 0.7. All of this considering the basic Force equation in terms of the energy considered:


$$
F = -\frac{dU}{dz} \therefore |F| = \left|\frac{dU}{dz}\right|
$$


## Conclusion 

The shape of a drop can be affected by some parameters, and to study the effect of each one Surface Evolver can be a very powerful tool. The contact angle for example, has the effect of increase or decrease the radius of the sphere that contains the drop. The gravity acts squishing the drop, and the volume of the drop acts in the shape of a drop in a way that for large volumes the drop is almost perfectly plane, while for small volumes the drop is pretty spherical. When a single point heterogeneity is at the surface of a drop it modifies its curvature and forces due to surface tension on it can be calculated with Surface Evolver. To sum up, Surface Evolver is a great Lab where surface phenomena can be investigated, numerically.