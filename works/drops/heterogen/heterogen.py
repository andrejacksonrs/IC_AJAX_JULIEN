
"""
Optimized way to scatter plot SE drop profiles and measure it's height
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy
from scipy import optimize
import pandas as pd
from scipy.interpolate import interp1d as intp
import glob


#------------------------------------------------------------------------------

#Import data from surface evolver 

matrix = pd.read_csv("z06.txt", sep = "\t", header = None).to_numpy()

#Define coordinates excluding the substrate vertices from the txt file

x = np.delete(matrix[:,1],[8,9,10,11,16])
y = np.delete(matrix[:,2],[8,9,10,11,16])
z = np.delete(matrix[:,3],[8,9,10,11,16])

r = np.sqrt(x*x + y*y)

r = np.concatenate((-r,r))
z = np.concatenate((z,z))

i = np.argsort(r)
r = r[i]
z = z[i]

plt.plot(r,z, label = "Energy = 49.07 ")

#plt.scatter(X,Z, color = "black", s = 5)

#------------------------------------------------------------------------------
#------------------------------------------------------------------------------

#Import data from surface evolver 

matrix = pd.read_csv("z07.txt", sep = "\t", header = None).to_numpy()

#Define coordinates excluding the substrate vertices from the txt file

x = np.delete(matrix[:,1],[8,9,10,11,16])
y = np.delete(matrix[:,2],[8,9,10,11,16])
z = np.delete(matrix[:,3],[8,9,10,11,16])

r = np.sqrt(x*x + y*y)

r = np.concatenate((-r,r))
z = np.concatenate((z,z))

i = np.argsort(r)
r = r[i]
z = z[i]

plt.plot(r,z,label = "Energy = 49.14")

#plt.scatter(X,Z, color = "black", s = 5)

#------------------------------------------------------------------------------
#Final export settings:

#Plot settings

#Title of x axis:
plt.xlabel('r axis')
#Title of y axis:
plt.ylabel('z axis')

plt.axes().set_aspect('equal')

plt.legend(loc='upper right', prop={'size': 6})

plt.xticks(np.linspace(-3,3,11))
plt.yticks(np.linspace(0,0.8,5))

plt.xlim(-3,3)
plt.ylim(0,0.8)

#plt.savefig('hetero.svg')