#All angles in one script

import numpy as np
import matplotlib.pyplot as plt
import scipy
from scipy import optimize
import pandas as pd


# 30 degrees

matrix = pd.read_csv("30d.txt",sep="\s+").to_numpy()

xdata30 = matrix[:,1]
ydata30 = matrix[:,2]
zdata30 = matrix[:,3]



rr = (xdata30 - 0.5)*(xdata30 - 0.5) + (ydata30 - 0.5)*(ydata30 - 0.5)

rdata30 = np.sqrt(rr)

#plt.scatter(rdata30, zdata30, color = "orange", s = 20, label = "x degrees" )








#fitting section

def f30(x,xc,yc,R):
    return yc + np.sqrt(R*R - (x-xc)*(x-xc))

guess = [0,0,max(rdata30)]

X30 = rdata30
Y30 = zdata30

params, params_covariance = scipy.optimize.curve_fit(f30,X30,Y30,guess)
params

xc30 = params[0]
yc30 = params[1]
R30 = params[2]


xvalues30 = np.linspace(0,max(rdata30),1000)
yvalues30 = f30(xvalues30,xc30,yc30,R30)

plt.plot(xvalues30,yvalues30, color = 'black', label = '30 degrees, R = 2,86')

# 60 degrees

matrix = pd.read_csv("60d.txt",sep="\s+").to_numpy()

xdata60 = matrix[:,1]
ydata60 = matrix[:,2]
zdata60 = matrix[:,3]

rr = (xdata60 - 0.5)*(xdata60 - 0.5) + (ydata60 - 0.5)*(ydata60 - 0.5)

rdata60 = np.sqrt(rr)

#plt.scatter(rdata60, zdata60, color = "orange", s = 20, label = "30 degrees" )



#fitting section

def f60(x,xc,yc,R):
    return yc + np.sqrt(R*R - (x-xc)*(x-xc))

guess60 = [0,0,max(rdata60)]

X60 = rdata60
Y60 = zdata60

params, params_covariance = scipy.optimize.curve_fit(f60,X60,Y60,guess60)
params

xc60 = params[0]
yc60 = params[1]
R60 = params[2]


xvalues60 = np.linspace(0,max(rdata60),1000)
yvalues60 = f60(xvalues60,xc60,yc60,R60)

plt.plot(xvalues60,yvalues60, color = 'blue', label = '60 degrees, R = 1,16')

# 90 degrees

matrix90 = pd.read_csv("90d.txt",sep="\s+").to_numpy()

xdata90 = matrix90[:,1]
ydata90 = matrix90[:,2]
zdata90 = matrix90[:,3]

rr90 = (xdata90 - 0.5)*(xdata90 - 0.5) + (ydata90 - 0.5)*(ydata90 - 0.5)

rdata90 = np.sqrt(rr90)

#plt.scatter(rdata90, zdata90, color = "orange", s = 20, label = "90 degrees" )



#fitting section

def f90(x,xc,yc,R):
    return yc + np.sqrt(R*R - (x-xc)*(x-xc))

guess90 = [0,0,max(rdata90)]

X90 = rdata90
Y90 = zdata90

params, params_covariance = scipy.optimize.curve_fit(f90,X90,Y90,guess90)
params

xc90 = params[0]
yc90 = params[1]
R90 = params[2]


xvalues90 = np.linspace(0,max(rdata90),1000)
yvalues90 = f90(xvalues90,xc90,yc90,R90)

plt.plot(xvalues90,yvalues90, color = 'green', label = '90 degrees, R = 0,78')




# 120 degrees

matrix120 = pd.read_csv("120d.txt",sep="\s+").to_numpy()

xdata120 = matrix120[:,1]
ydata120 = matrix120[:,2]
zdata120 = matrix120[:,3]

rr120 = (xdata120 - 0.5)*(xdata120 - 0.5) + (ydata120 - 0.5)*(ydata120 - 0.5)

rdata120 = np.sqrt(rr120)

#plt.scatter(rdata120, zdata120, color = "orange", s = 20, label = "120 degrees" )



#fitting section

def f120(x,xc,yc,R):
    return yc + np.sqrt(R*R - (x-xc)*(x-xc))

guess120 = [0,0,max(zdata120)]

X120 = zdata120
Y120 = rdata120

params, params_covariance = scipy.optimize.curve_fit(f120,X120,Y120,guess120)
params

xc120 = params[0]
yc120 = params[1]
R120 = params[2]

zero120 = xc120 + np.sqrt(R120*R120 - yc120*yc120)

xvalues120 = np.linspace(min(zdata120),zero120,1000)
yvalues120 = f120(xvalues120,xc120,yc120,R120)

plt.plot(yvalues120,xvalues120, color = 'orange', label = '120 degrees, R = 0,66')


# 150 degrees

matrix150 = pd.read_csv("150d.txt",sep="\s+").to_numpy()

xdata150 = matrix150[:,1]
ydata150 = matrix150[:,2]
zdata150 = matrix150[:,3]

rr150 = (xdata150 - 0.5)*(xdata150 - 0.5) + (ydata150 - 0.5)*(ydata150 - 0.5)

rdata150 = np.sqrt(rr150)

#plt.scatter(rdata150, zdata150, color = "orange", s = 20, label = "150 degrees" )



#fitting section

def f150(x,xc,yc,R):
    return yc + np.sqrt(R*R - (x-xc)*(x-xc))

guess150 = [0,0,max(zdata150)]

X150 = zdata150
Y150 = rdata150

params, params_covariance = scipy.optimize.curve_fit(f150,X150,Y150,guess150)
params

xc150 = params[0]
yc150 = params[1]
R150 = params[2]

zero150 = xc150 + np.sqrt(R150*R150 - yc150*yc150)

xvalues150 = np.linspace(min(zdata150),zero150,1000)
yvalues150 = f150(xvalues150,xc150,yc150,R150)

plt.plot(yvalues150,xvalues150, color = 'red', label = '150 degrees, R = 0,62')




#Plot Settings



plt.legend(loc='upper right', prop={'size': 8})

plt.axes().set_aspect('equal')

plt.ylim(0,1.25)
plt.xlim(0,1.4)

#Title of x axis:
plt.xlabel('Width (Surface Evolver unit length)')
#Title of y axis:
plt.ylabel('Height (Surface Evolver unit length)')

plt.title('Drop profile depending on the Contact angle', fontsize = 10)

print([R30,R60,R90,R120,R150])

plt.savefig('Drop_profiles.svg')


