"""
Variation of contact angle analysys optimized version - just for 1 .txt

Author: André Jackson Ramos Simões

THIS SCRIPT IS NO FINISHED YET
"""


import numpy as np
import matplotlib.pyplot as plt
import scipy
from scipy import optimize
import pandas as pd
from scipy.interpolate import interp1d as intp
import glob

#--------------------X DEGREES------------------------------------

matrix = pd.read_csv('120.txt', sep = "\t", header = None).to_numpy()
        
#Define coordinates excluding the substrate vertices from the txt file
        
x = np.delete(matrix[:,1],[8,9,10,11,17])
y = np.delete(matrix[:,2],[8,9,10,11,17])
z = np.delete(matrix[:,3],[8,9,10,11,17])
        
        
#SORT X AND Z
        
s = np.size(x)
        
X = np.array([])
#print(X)
        
Z = np.array([])
#print(Z)
        
        
for i in range(s):
    if y[i] == 0 and x[i] >= 0:
        X = np.append(X,x[i])
        Z = np.append(Z,z[i])
                
i = np.argsort(Z)
X = X[i]
Z = Z[i]
        
#measure the height of the drop

plt.axes().set_aspect('equal')      
  
# Regular plot (X,Z)
#plt.plot(X,Z, label = "V = 1000")
        
plt.scatter(X,Z, color = "black", s = 10)


# Fit for theta <= 90
"""
def f30(x,xc,yc,R):
    return yc + np.sqrt(R*R - (x-xc)*(x-xc))

guess = [0,0,max(X)]

X30 = X
Y30 = Z

params, params_covariance = scipy.optimize.curve_fit(f30,X30,Y30,guess)
params

xc30 = params[0]
yc30 = params[1]
R30 = params[2]


xvalues30 = np.linspace(0,max(X),1000)
yvalues30 = f30(xvalues30,xc30,yc30,R30)

plt.plot(xvalues30,yvalues30, color = 'black', label = '30 degrees, R = 2,86')
"""

#fit for theta > 90

def f120(x,xc,yc,R):
    return yc + np.sqrt(R*R - (x-xc)*(x-xc))

guess120 = [0,0,max(Z)]

X120 = Z
Y120 = X

params, params_covariance = scipy.optimize.curve_fit(f120,X120,Y120,guess120)
params

xc120 = params[0]
yc120 = params[1]
R120 = params[2]

zero120 = xc120 + np.sqrt(R120*R120 - yc120*yc120)

xvalues120 = np.linspace(min(Z),zero120,1000)
yvalues120 = f120(xvalues120,xc120,yc120,R120)

plt.plot(yvalues120,xvalues120, color = 'orange', label = '120 degrees, R = 0,66')


















"""
def f(x,R,c):
    return c + np.sqrt(R*R - x*x)

guess = [1,1]

params, params_covariance = scipy.optimize.curve_fit(f,X,Z,guess)
params

R = params[0]
c = params[1]


xvalues = np.linspace(0,max(X),1000)
zvalues = f(xvalues,R,c)

plt.plot(xvalues,zvalues, color = 'black', label = '30 degrees, R =')
"""

"""
#Inverted plot

plt.plot(Z,X, label = "V = 1000")
        
plt.scatter(Z,X, color = "black", s = 10)
"""

#------------------------------------------------------------------------








