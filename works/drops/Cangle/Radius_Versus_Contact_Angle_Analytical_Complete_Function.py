"""
Contact Angle x Radius Curve
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy
from scipy import optimize

#CAd = np.array([30.0,60.0,90.0,120.0,150.0])
CA = np.array([3.1415/6 , 3.1415/3, 3.1415/2, 6.2830/3, 5*3.1415/6])
cos = np.cos(CA)
R = np.array([2.8619831541968885, 1.1592015807269838, 0.7821831365722481, 0.6559323488598537, 0.6246972428821442])
logR = np.log(R)
logCA = np.log(CA)

#fitting section

def f(x,a,b):
    return a*pow(((2+np.cos(x))*pow((1-np.cos(x)),2)),b)

guess = [1,1]

X = CA
Y = R

params, params_covariance = scipy.optimize.curve_fit(f,X,Y,guess)
params

a = params[0]
b = params[1]

xvalues = np.linspace(min(X),max(X),1000)
yvalues = f(xvalues,a,b)

plt.plot(xvalues,yvalues, color = 'deepskyblue', label = 'Fit: a = 0.99, b = -0.35')

plt.scatter(CA,R,s = 30, color = 'black', label = 'Radius data from Surface Evolver')







#Plot Settings 

plt.legend(loc='upper right', prop={'size': 10})

#plt.axes().set_aspect('equal')

#plt.ylim(min(Y),max(Y))
#plt.xlim(min(X),max(X))

plt.xlim(0.4,2.75)
plt.ylim(0.5,3)

plt.xticks(np.linspace(0.5,2.75,10))
plt.yticks(np.linspace(0.5,3.0,11))

#Title of x axis:
plt.xlabel('Contact Angle (rad)')
#Title of y axis:
plt.ylabel('Radius (Surface Evolver unit length)')

plt.title('Radius of the drop in function of the contact angle', fontsize = 13)

#plt.savefig("Radius_Versus_Contact_Angle_Perfect_fit.svg")

