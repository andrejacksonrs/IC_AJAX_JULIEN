"""
How to delete the vertices of the substrate that comes in the SE 
exported .txt, to analise just the drop surface vertices
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy
from scipy import optimize
import pandas as pd
from scipy.interpolate import interp1d as intp
import glob


#Import data from surface evolver 

matrix = pd.read_csv("1.txt", sep = "\t", header = None).to_numpy()

#Define coordinates excluding the substrate vertices from the txt file

x = np.delete(matrix[:,1],[8,9,10,11,17])
y = np.delete(matrix[:,2],[8,9,10,11,17])
z = np.delete(matrix[:,3],[8,9,10,11,17])
    
plt.scatter(x,z)


