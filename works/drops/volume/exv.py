"""
Thickness of the drops with different volumes, with 
g = 10 and theta = 90

"""

import numpy as np
import matplotlib.pyplot as plt
import scipy
from scipy import optimize
import pandas as pd
from scipy.interpolate import interp1d as intp

E = np.array([0.077618, 0.10458,  0.13056,  0.162141, 0.213655, 0.25954,  0.309922, 0.379414,
 0.426891, 0.46296,  0.484533, 0.48418,  0.477032, 0.468608, 0.463031, 0.459713, 0.456826, 0.456236, 0.454314])

v = np.array([0.001, 0.0025, 0.005, 0.01, 0.025, 0.05, 0.1, 0.25, 0.5, 1, 2.5, 5, 10, 25, 50, 100, 250, 500, 1000])




#-----------------------------------------


#-----------------------------------------------

#FINAL SETTINGS LOG PLOT

#plt.plot(g,E,color = "black")

#Plot settings

#Title of x axis:
plt.xlabel('Volume V')
#Title of y axis:
plt.ylabel('Height H')

plt.grid(True, which="both")

plt.xscale('log')

plt.scatter(v,E, color = "black")
plt.plot(v,E, color = "dodgerblue")

plt.xlim(0.0005,1015)
plt.ylim(0,0.5)

#plt.savefig('exv.svg')


#-----------------------------------------------
"""
#FINAL SETTINGS REGULAR PLOT

plt.scatter(v,E)
plt.xticks(np.linspace(0,10,11))
"""
