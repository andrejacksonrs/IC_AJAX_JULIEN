"""
How to delete elements from an array
"""
import numpy as np

# Defines an np array of floats 64
v = np.array([1.0,2.0,3.0,4.0,5.0])

# Redefines the array v as the v but without the element 0,1,2 and 3 of the v array
v = np.delete(v,[0,1,2,3])

