"""
Volume effects
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy
from scipy import optimize
import pandas as pd
from scipy.interpolate import interp1d as intp


#STEP 1: Import data from surface evolver 

"""
OBS: make sure to delete vertices 9,10,11,12,17 and 18 from the .txt file 
exported from Surface Evolver, because they belong to the substrate plane, not 
to the drop surface.
"""
matrix = pd.read_csv("005.txt",sep="\s+").to_numpy()

x = matrix[:,1]
y = matrix[:,2]
z = matrix[:,3]

x = x -0.5
y = y - 0.5

s = np.size(x)

X = np.array([])
#print(X)

Z = np.array([])
#print(Z)


for i in range(s):
    if y[i] == 0:
        X = np.append(X,x[i])
        Z = np.append(Z,z[i])
        
i = np.argsort(X)
X = X[i]
Z = Z[i]


#INTERPOLATION SECTION
linear_interp = intp(X,Z)

#Here is the control of the  interpolated points, the last number in the 
#linspace defines how many interpolations will be done, and the 2 first numbers
#define the interval in witch the interpolation will occur.
xvalues = np.linspace(-0.01, 0.01, 3)

linear_results = linear_interp(xvalues)

e0 = max(linear_results)

print(e0)

plt.plot(X,Z, color = "red", label = "V = 0.05")

#plt.scatter(X,Z, color = "black")
#plt.scatter(xvalues,linear_results, color = "black")

#---------------------------------------

#STEP 1: Import data from surface evolver 

"""
OBS: make sure to delete vertices 9,10,11,12,17 and 18 from the .txt file 
exported from Surface Evolver, because they belong to the substrate plane, not 
to the drop surface.
"""
matrix = pd.read_csv("01.txt",sep="\s+").to_numpy()

x = matrix[:,1]
y = matrix[:,2]
z = matrix[:,3]

x = x -0.5
y = y - 0.5

s = np.size(x)

X = np.array([])
#print(X)

Z = np.array([])
#print(Z)


for i in range(s):
    if y[i] == 0:
        X = np.append(X,x[i])
        Z = np.append(Z,z[i])
        
i = np.argsort(X)
X = X[i]
Z = Z[i]

#INTERPOLATION SECTION
linear_interp = intp(X,Z)

#Here is the control of the  interpolated points, the last number in the 
#linspace defines how many interpolations will be done, and the 2 first numbers
#define the interval in witch the interpolation will occur.
xvalues = np.linspace(-0.01, 0.01, 3)

linear_results = linear_interp(xvalues)

emo = max(linear_results)

print(emo)

plt.plot(X,Z,color = "orange", label = "V = 0.1")
#plt.scatter(X,Z, color = "grey")
#plt.scatter(xvalues,linear_results, color = "grey")


#---------------------------------------

#STEP 1: Import data from surface evolver 

"""
OBS: make sure to delete vertices 9,10,11,12,17 and 18 from the .txt file 
exported from Surface Evolver, because they belong to the substrate plane, not 
to the drop surface.
"""
matrix = pd.read_csv("02.txt",sep="\s+").to_numpy()

x = matrix[:,1]
y = matrix[:,2]
z = matrix[:,3]

x = x -0.5
y = y - 0.5

s = np.size(x)

X = np.array([])
#print(X)

Z = np.array([])
#print(Z)


for i in range(s):
    if y[i] == 0:
        X = np.append(X,x[i])
        Z = np.append(Z,z[i])
        
i = np.argsort(X)
X = X[i]
Z = Z[i]

plt.plot(X,Z, color = "yellow", label = "V = 0.2")

#INTERPOLATION SECTION
linear_interp = intp(X,Z)

#Here is the control of the  interpolated points, the last number in the 
#linspace defines how many interpolations will be done, and the 2 first numbers
#define the interval in witch the interpolation will occur.
xvalues = np.linspace(-0.01, 0.01, 3)

linear_results = linear_interp(xvalues)

ema = max(linear_results)

print(ema)

#plt.scatter(X,Z, color = "red")
#plt.scatter(xvalues,linear_results, color = "red")

#---------------------------------------

#STEP 1: Import data from surface evolver 

"""
OBS: make sure to delete vertices 9,10,11,12,17 and 18 from the .txt file 
exported from Surface Evolver, because they belong to the substrate plane, not 
to the drop surface.
"""
matrix = pd.read_csv("05.txt",sep="\s+").to_numpy()

x = matrix[:,1]
y = matrix[:,2]
z = matrix[:,3]

x = x -0.5
y = y - 0.5

s = np.size(x)

X = np.array([])
#print(X)

Z = np.array([])
#print(Z)


for i in range(s):
    if y[i] == 0:
        X = np.append(X,x[i])
        Z = np.append(Z,z[i])
        
i = np.argsort(X)
X = X[i]
Z = Z[i]

plt.plot(X,Z, color = "green", label = "V = 0.5")

#INTERPOLATION SECTION
linear_interp = intp(X,Z)

#Here is the control of the  interpolated points, the last number in the 
#linspace defines how many interpolations will be done, and the 2 first numbers
#define the interval in witch the interpolation will occur.
xvalues = np.linspace(-0.01, 0.01, 3)

linear_results = linear_interp(xvalues)

ee = max(linear_results)

print(ee)

#plt.scatter(X,Z, color = "blue")
#plt.scatter(xvalues,linear_results, color = "blue")

#---------------------------------------

#STEP 1: Import data from surface evolver 

"""
OBS: make sure to delete vertices 9,10,11,12,17 and 18 from the .txt file 
exported from Surface Evolver, because they belong to the substrate plane, not 
to the drop surface.
"""
matrix = pd.read_csv("1.txt",sep="\s+").to_numpy()

x = matrix[:,1]
y = matrix[:,2]
z = matrix[:,3]

x = x -0.5
y = y - 0.5

s = np.size(x)

X = np.array([])
#print(X)

Z = np.array([])
#print(Z)


for i in range(s):
    if y[i] == 0:
        X = np.append(X,x[i])
        Z = np.append(Z,z[i])
        
i = np.argsort(X)
X = X[i]
Z = Z[i]

plt.plot(X,Z,color = "blue", label = "V = 1")

#INTERPOLATION SECTION
linear_interp = intp(X,Z)

#Here is the control of the  interpolated points, the last number in the 
#linspace defines how many interpolations will be done, and the 2 first numbers
#define the interval in witch the interpolation will occur.
xvalues = np.linspace(-0.01, 0.01, 3)

linear_results = linear_interp(xvalues)

ej = max(linear_results)

print(ej)

#plt.scatter(X,Z, color = "orange")
#plt.scatter(xvalues,linear_results, color = "orange")

#---------------------------------------

#STEP 1: Import data from surface evolver 

"""
OBS: make sure to delete vertices 9,10,11,12,17 and 18 from the .txt file 
exported from Surface Evolver, because they belong to the substrate plane, not 
to the drop surface.
"""
matrix = pd.read_csv("2.txt",sep="\s+").to_numpy()

x = matrix[:,1]
y = matrix[:,2]
z = matrix[:,3]

x = x -0.5
y = y - 0.5

s = np.size(x)

X = np.array([])
#print(X)

Z = np.array([])
#print(Z)


for i in range(s):
    if y[i] == 0:
        X = np.append(X,x[i])
        Z = np.append(Z,z[i])
        
i = np.argsort(X)
X = X[i]
Z = Z[i]

plt.plot(X,Z,color = "purple", label = "V = 2")

#INTERPOLATION SECTION
linear_interp = intp(X,Z)

#Here is the control of the  interpolated points, the last number in the 
#linspace defines how many interpolations will be done, and the 2 first numbers
#define the interval in witch the interpolation will occur.
xvalues = np.linspace(-0.01, 0.01, 3)

linear_results = linear_interp(xvalues)

ej = max(linear_results)

print(ej)

#plt.scatter(X,Z, color = "orange")
#plt.scatter(xvalues,linear_results, color = "orange")

#---------------------------------------

#STEP 1: Import data from surface evolver 

"""
OBS: make sure to delete vertices 9,10,11,12,17 and 18 from the .txt file 
exported from Surface Evolver, because they belong to the substrate plane, not 
to the drop surface.
"""
matrix = pd.read_csv("3.txt",sep="\s+").to_numpy()

x = matrix[:,1]
y = matrix[:,2]
z = matrix[:,3]

x = x -0.5
y = y - 0.5

s = np.size(x)

X = np.array([])
#print(X)

Z = np.array([])
#print(Z)


for i in range(s):
    if y[i] == 0:
        X = np.append(X,x[i])
        Z = np.append(Z,z[i])
        
i = np.argsort(X)
X = X[i]
Z = Z[i]

plt.plot(X,Z,color = "silver", label = "V = 3")

#INTERPOLATION SECTION
linear_interp = intp(X,Z)

#Here is the control of the  interpolated points, the last number in the 
#linspace defines how many interpolations will be done, and the 2 first numbers
#define the interval in witch the interpolation will occur.
xvalues = np.linspace(-0.01, 0.01, 3)

linear_results = linear_interp(xvalues)

ej = max(linear_results)

print(ej)

#plt.scatter(X,Z, color = "orange")
#plt.scatter(xvalues,linear_results, color = "orange")

#---------------------------------------

#STEP 1: Import data from surface evolver 

"""
OBS: make sure to delete vertices 9,10,11,12,17 and 18 from the .txt file 
exported from Surface Evolver, because they belong to the substrate plane, not 
to the drop surface.
"""
matrix = pd.read_csv("4.txt",sep="\s+").to_numpy()

x = matrix[:,1]
y = matrix[:,2]
z = matrix[:,3]

x = x -0.5
y = y - 0.5

s = np.size(x)

X = np.array([])
#print(X)

Z = np.array([])
#print(Z)


for i in range(s):
    if y[i] == 0:
        X = np.append(X,x[i])
        Z = np.append(Z,z[i])
        
i = np.argsort(X)
X = X[i]
Z = Z[i]

plt.plot(X,Z,color = "grey", label = "V = 4")

#INTERPOLATION SECTION
linear_interp = intp(X,Z)

#Here is the control of the  interpolated points, the last number in the 
#linspace defines how many interpolations will be done, and the 2 first numbers
#define the interval in witch the interpolation will occur.
xvalues = np.linspace(-0.01, 0.01, 3)

linear_results = linear_interp(xvalues)

ej = max(linear_results)

print(ej)

#plt.scatter(X,Z, color = "orange")
#plt.scatter(xvalues,linear_results, color = "orange")

#---------------------------------------

#STEP 1: Import data from surface evolver 

"""
OBS: make sure to delete vertices 9,10,11,12,17 and 18 from the .txt file 
exported from Surface Evolver, because they belong to the substrate plane, not 
to the drop surface.
"""
matrix = pd.read_csv("5.txt",sep="\s+").to_numpy()

x = matrix[:,1]
y = matrix[:,2]
z = matrix[:,3]


s = np.size(x)

X = np.array([])
#print(X)

Z = np.array([])
#print(Z)


for i in range(s):
    if y[i] == 0:
        X = np.append(X,x[i])
        Z = np.append(Z,z[i])
        
i = np.argsort(X)
X = X[i]
Z = Z[i]

plt.plot(X,Z,color = "black", label = "V = 5")

#INTERPOLATION SECTION
linear_interp = intp(X,Z)

#Here is the control of the  interpolated points, the last number in the 
#linspace defines how many interpolations will be done, and the 2 first numbers
#define the interval in witch the interpolation will occur.
xvalues = np.linspace(-0.01, 0.01, 3)

linear_results = linear_interp(xvalues)

ej = max(linear_results)

print(ej)

#plt.scatter(X,Z, color = "orange")
#plt.scatter(xvalues,linear_results, color = "orange")




#---------------------------------------

#---------------------------------------

#---------------------------------------

#FINAL SETTINGS

#Plot settings



#Title of x axis:
plt.xlabel('x axis')
#Title of y axis:
plt.ylabel('z axis')

plt.axes().set_aspect('equal')

plt.legend(loc='upper right', prop={'size': 4})

plt.xticks(np.linspace(-2.5,2.5,11))

plt.xlim(-2.5,2.5)
plt.ylim(0,1.0)

#ge = gravity effects
plt.savefig('ve.svg')
