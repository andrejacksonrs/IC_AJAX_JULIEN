import numpy as np
import matplotlib.pyplot as plt
import scipy
from scipy import optimize
import pandas as pd
from scipy.interpolate import interp1d as intp
import glob

files = glob.glob("*.txt")

L = len(files)

volume = []

for i in range(L):
    volume.append(files[i][:-4])
    
volume.sort(key=float)

"""
del volume[1]
del volume[1]
del volume[2]
del volume[3]
"""

L = len(volume)
    
e = np.array([])



matrix = pd.read_csv('1000.txt', sep = "\t", header = None).to_numpy()
        
#Define coordinates excluding the substrate vertices from the txt file
        
x = np.delete(matrix[:,1],[8,9,10,11,17])
y = np.delete(matrix[:,2],[8,9,10,11,17])
z = np.delete(matrix[:,3],[8,9,10,11,17])
        
        
#SORT X AND Z
        
s = np.size(x)
        
X = np.array([])
#print(X)
        
Z = np.array([])
#print(Z)
        
        
for i in range(s):
    if y[i] == 0:
        X = np.append(X,x[i])
        Z = np.append(Z,z[i])
                
i = np.argsort(X)
X = X[i]
Z = Z[i]
        
#measure the height of the drop
        
e = np.append(e,max(z))
        
plt.plot(X,Z, label = "V = 1000")
        
plt.scatter(X,Z, color = "black", s = 5)
