"""
Thickness of the drops under the action of different
gravitational fields

0.51,
1.62
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy
from scipy import optimize
import pandas as pd
from scipy.interpolate import interp1d as intp

E = np.array([0.56,0.51,0.45,0.34,0.21])
g = np.array([0,1.62,3.71,9.81,24.79])

plt.scatter(g[0],E[0],label = "g = 0",color = "black", s =100)
plt.scatter(g[1],E[1],label = "Moon",color = "grey", s =100)
plt.scatter(g[2],E[2],label = "Mars",color = "red", s =100)
plt.scatter(g[3],E[3],label = "Earth",color = "blue", s =100)
plt.scatter(g[4],E[4],label = "Jupiter",color = "orange", s =100)

x = g
y = E

#-----------------------------------------


#-----------------------------------------------

#FINAL SETTINGS

#plt.plot(g,E,color = "black")

#Plot settings

plt.title('How gravity changes the thickness of drops')

#Title of x axis:
plt.xlabel('Gravity constant g')
#Title of y axis:
plt.ylabel('Thickness e')

plt.legend(loc='upper right', prop={'size': 12})

plt.xticks(np.linspace(0,25,6))
plt.yticks(np.linspace(0,0.6,7))

plt.savefig('EExg.svg')