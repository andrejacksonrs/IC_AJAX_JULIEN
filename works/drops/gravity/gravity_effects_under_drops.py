"""
Gravity effects on drops script
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy
from scipy import optimize
import pandas as pd
from scipy.interpolate import interp1d as intp

# g = 0


#STEP 1: Import data from surface evolver 

"""
OBS: make sure to delete vertices 9,10,11,12,17 and 18 from the .txt file 
exported from Surface Evolver, because they belong to the substrate plane, not 
to the drop surface.
"""
matrix = pd.read_csv("jg.txt",sep="\s+").to_numpy()

xdata0g = matrix[:,1]
ydata0g = matrix[:,2]
zdata0g = matrix[:,3]





#STEP 2: Process surface evolver data to organize it


#Merging arrays into one to plot the whole drop profile

Zdata0g = np.concatenate((zdata0g,zdata0g))

#Sorting arrays
i = np.argsort(xdata0g)
X0g = xdata0g[i]
Z0g = zdata0g[i]





#STEP 3: measurements

#Measure the radius of the drop

RADIUS0g = max(X0g)

#interpolation to measure the thickness e of the drop

linear_interp = intp(X0g,Z0g)

xvalues = np.linspace(-0.2, 0.2, 100)

linear_results = linear_interp(xvalues)

#measure the thickness of the drop
e0g = max(linear_results)






#STEP 4: PLOTTING AND PRINTING

#scatter plot the drop profile
#plt.scatter(R0g, Z0g, color = "red", s = 5, label = 'g = 0')

#line plot of the drop profile
plt.plot(X0g,Z0g, color = 'black', label = 'g = 0')

#plot the interpolation points
#plt.scatter(xvalues,linear_results, label = 'interpolation', color = 'red', s = 5)

#printing measurements

print('Thickness:') 
print(e0g)

print('Radius:')
print(RADIUS0g)

"""

# g = 5


#STEP 1: Import data from surface evolver 


"""
"""
matrix = pd.read_csv("5g.txt",sep="\s+").to_numpy()

xdata5g = matrix[:,1]
ydata5g = matrix[:,2]
zdata5g = matrix[:,3]





#STEP 2: Process surface evolver data to organize it

#Define r coordinate of points
rr = (xdata5g - 0.5)*(xdata5g - 0.5) + (ydata5g - 0.5)*(ydata5g - 0.5)

rdata5g = np.sqrt(rr)

#Merging arrays into one to plot the whole drop profile
Rdata5g = np.concatenate((-rdata5g,rdata5g))
Zdata5g = np.concatenate((zdata5g,zdata5g))

#Sorting arrays
i = np.argsort(Rdata5g)
R5g = Rdata5g[i]
Z5g = Zdata5g[i]





#STEP 3: measurements

#Measure the radius of the drop

RADIUS5g = max(R5g)

#interpolation to measure the thickness e of the drop

linear_interp = intp(R5g,Z5g)

xvalues = np.linspace(-0.2, 0.2, 100)

linear_results = linear_interp(xvalues)

#measure the thickness of the drop
e5g = max(linear_results)






#STEP 4: PLOTTING AND PRINTING

#scatter plot the drop profile
#plt.scatter(R5g, Z5g, color = "red", s = 5, label = 'g = 5')

#line plot of the drop profile
plt.plot(R5g,Z5g, color = 'orange', label = 'g = 5')

#plot the interpolation points
#plt.scatter(xvalues,linear_results, label = 'interpolation', color = 'red', s = 5)

#printing measurements

print('Thickness:') 
print(e5g)

print('Radius:')
print(RADIUS5g)


# g = 10


#STEP 1: Import data from surface evolver 

"""
"""
matrix = pd.read_csv("10g.txt",sep="\s+").to_numpy()

xdata10g = matrix[:,1]
ydata10g = matrix[:,2]
zdata10g = matrix[:,3]





#STEP 2: Process surface evolver data to organize it

#Define r coordinate of points
rr = (xdata10g - 0.5)*(xdata10g - 0.5) + (ydata10g - 0.5)*(ydata10g - 0.5)

rdata10g = np.sqrt(rr)

#Merging arrays into one to plot the whole drop profile
Rdata10g = np.concatenate((-rdata10g,rdata10g))
Zdata10g = np.concatenate((zdata10g,zdata10g))

#Sorting arrays
i = np.argsort(Rdata10g)
R10g = Rdata10g[i]
Z10g = Zdata10g[i]





#STEP 3: measurements

#Measure the radius of the drop

RADIUS10g = max(R10g)

#interpolation to measure the thickness e of the drop

linear_interp = intp(R10g,Z10g)

xvalues = np.linspace(-0.2, 0.2, 100)

linear_results = linear_interp(xvalues)

#measure the thickness of the drop
e10g = max(linear_results)






#STEP 4: PLOTTING AND PRINTING

#scatter plot the drop profile
#plt.scatter(R10g, Z10g, color = "red", s = 5, label = 'g = 10')

#line plot of the drop profile
plt.plot(R10g,Z10g, color = 'red', label = 'g = 10')

#plot the interpolation points
#plt.scatter(xvalues,linear_results, label = 'interpolation', color = 'red', s = 5)

#printing measurements

print('Thickness:') 
print(e10g)

print('Radius:')
print(RADIUS10g)

# g = 15


#STEP 1: Import data from surface evolver 

"""
"""
matrix = pd.read_csv("15g.txt",sep="\s+").to_numpy()

xdata15g = matrix[:,1]
ydata15g = matrix[:,2]
zdata15g = matrix[:,3]





#STEP 2: Process surface evolver data to organize it

#Define r coordinate of points
rr = (xdata15g - 0.5)*(xdata15g - 0.5) + (ydata15g - 0.5)*(ydata15g - 0.5)

rdata15g = np.sqrt(rr)

#Merging arrays into one to plot the whole drop profile
Rdata15g = np.concatenate((-rdata15g,rdata15g))
Zdata15g = np.concatenate((zdata15g,zdata15g))

#Sorting arrays
i = np.argsort(Rdata15g)
R15g = Rdata15g[i]
Z15g = Zdata15g[i]





#STEP 3: measurements

#Measure the radius of the drop

RADIUS15g = max(R15g)

#interpolation to measure the thickness e of the drop

linear_interp = intp(R15g,Z15g)

xvalues = np.linspace(-0.2, 0.2, 100)

linear_results = linear_interp(xvalues)

#measure the thickness of the drop
e15g = max(linear_results)






#STEP 4: PLOTTING AND PRINTING

#scatter plot the drop profile
#plt.scatter(R15g, Z15g, color = "red", s = 5, label = 'g = 15')

#line plot of the drop profile
plt.plot(R15g,Z15g, color = 'green', label = 'g = 15')

#plot the interpolation points
#plt.scatter(xvalues,linear_results, label = 'interpolation', color = 'red', s = 5)

#printing measurements

print('Thickness:') 
print(e15g)

print('Radius:')
print(RADIUS15g)









# g = 20


#STEP 1: Import data from surface evolver 

"""
"""
matrix = pd.read_csv("20g.txt",sep="\s+").to_numpy()

xdata20g = matrix[:,1]
ydata20g = matrix[:,2]
zdata20g = matrix[:,3]





#STEP 2: Process surface evolver data to organize it

#Define r coordinate of points
rr = (xdata20g - 0.5)*(xdata20g - 0.5) + (ydata20g - 0.5)*(ydata20g - 0.5)

rdata20g = np.sqrt(rr)

#Merging arrays into one to plot the whole drop profile
Rdata20g = np.concatenate((-rdata20g,rdata20g))
Zdata20g = np.concatenate((zdata20g,zdata20g))

#Sorting arrays
i = np.argsort(Rdata20g)
R20g = Rdata20g[i]
Z20g = Zdata20g[i]





#STEP 3: measurements

#Measure the radius of the drop

RADIUS20g = max(R20g)

#interpolation to measure the thickness e of the drop

linear_interp = intp(R20g,Z20g)

xvalues = np.linspace(-0.2, 0.2, 100)

linear_results = linear_interp(xvalues)

#measure the thickness of the drop
e20g = max(linear_results)






#STEP 4: PLOTTING AND PRINTING

#scatter plot the drop profile
#plt.scatter(R20g, Z20g, color = "red", s = 5, label = 'g = 20')

#line plot of the drop profile
plt.plot(R20g,Z20g, color = 'blue', label = 'g = 20')

#plot the interpolation points
#plt.scatter(xvalues,linear_results, label = 'interpolation', color = 'red', s = 5)

#printing measurements

print('Thickness:') 
print(e20g)

print('Radius:')
print(RADIUS20g)
"""


#FINAL SETTINGS

#Plot settings

plt.title('Drop profiles interpolated')

#Title of x axis:
plt.xlabel('Width')
#Title of y axis:
plt.ylabel('Height')

plt.axes().set_aspect('equal')

plt.legend(loc='upper right', prop={'size': 5})

plt.xticks(np.linspace(-1.5,1.5,7))

plt.xlim(-1.5,1.5)
plt.ylim(0,0.8)

#DPVG = DROP PROFILES VARYING GRAVITY
#plt.savefig('DPVG.svg')

