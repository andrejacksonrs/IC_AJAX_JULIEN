"""
Gravity effects simpler script
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy
from scipy import optimize
import pandas as pd
from scipy.interpolate import interp1d as intp


#STEP 1: Import data from surface evolver 

"""
OBS: make sure to delete vertices 9,10,11,12,17 and 18 from the .txt file 
exported from Surface Evolver, because they belong to the substrate plane, not 
to the drop surface.
"""
matrix = pd.read_csv("0g.txt",sep="\s+").to_numpy()

x = matrix[:,1]
y = matrix[:,2]
z = matrix[:,3]

x = x -0.5
y = y - 0.5

s = np.size(x)

X = np.array([])
#print(X)

Z = np.array([])
#print(Z)


for i in range(s):
    if y[i] == 0:
        X = np.append(X,x[i])
        Z = np.append(Z,z[i])
        
i = np.argsort(X)
X = X[i]
Z = Z[i]


#INTERPOLATION SECTION
linear_interp = intp(X,Z)

#Here is the control of the  interpolated points, the last number in the 
#linspace defines how many interpolations will be done, and the 2 first numbers
#define the interval in witch the interpolation will occur.
xvalues = np.linspace(-0.995096, 0.995096, 3)

linear_results = linear_interp(xvalues)

e0 = max(linear_results)

print(e0)

plt.plot(X,Z, color = "black", label = "g = 0")

#plt.scatter(X,Z, color = "black")
#plt.scatter(xvalues,linear_results, color = "black")

#---------------------------------------

#STEP 1: Import data from surface evolver 

"""
OBS: make sure to delete vertices 9,10,11,12,17 and 18 from the .txt file 
exported from Surface Evolver, because they belong to the substrate plane, not 
to the drop surface.
"""
matrix = pd.read_csv("mog.txt",sep="\s+").to_numpy()

x = matrix[:,1]
y = matrix[:,2]
z = matrix[:,3]

x = x -0.5
y = y - 0.5

s = np.size(x)

X = np.array([])
#print(X)

Z = np.array([])
#print(Z)


for i in range(s):
    if y[i] == 0:
        X = np.append(X,x[i])
        Z = np.append(Z,z[i])
        
i = np.argsort(X)
X = X[i]
Z = Z[i]

#INTERPOLATION SECTION
linear_interp = intp(X,Z)

#Here is the control of the  interpolated points, the last number in the 
#linspace defines how many interpolations will be done, and the 2 first numbers
#define the interval in witch the interpolation will occur.
xvalues = np.linspace(-0.995096, 0.995096, 3)

linear_results = linear_interp(xvalues)

emo = max(linear_results)

print(emo)

plt.plot(X,Z,color = "grey", label = "Moon")
#plt.scatter(X,Z, color = "grey")
#plt.scatter(xvalues,linear_results, color = "grey")


#---------------------------------------

#STEP 1: Import data from surface evolver 

"""
OBS: make sure to delete vertices 9,10,11,12,17 and 18 from the .txt file 
exported from Surface Evolver, because they belong to the substrate plane, not 
to the drop surface.
"""
matrix = pd.read_csv("mag.txt",sep="\s+").to_numpy()

x = matrix[:,1]
y = matrix[:,2]
z = matrix[:,3]

x = x -0.5
y = y - 0.5

s = np.size(x)

X = np.array([])
#print(X)

Z = np.array([])
#print(Z)


for i in range(s):
    if y[i] == 0:
        X = np.append(X,x[i])
        Z = np.append(Z,z[i])
        
i = np.argsort(X)
X = X[i]
Z = Z[i]

plt.plot(X,Z, color = "red", label = "Mars")

#INTERPOLATION SECTION
linear_interp = intp(X,Z)

#Here is the control of the  interpolated points, the last number in the 
#linspace defines how many interpolations will be done, and the 2 first numbers
#define the interval in witch the interpolation will occur.
xvalues = np.linspace(-0.995096, 0.995096, 3)

linear_results = linear_interp(xvalues)

ema = max(linear_results)

print(ema)

#plt.scatter(X,Z, color = "red")
#plt.scatter(xvalues,linear_results, color = "red")

#---------------------------------------

#STEP 1: Import data from surface evolver 

"""
OBS: make sure to delete vertices 9,10,11,12,17 and 18 from the .txt file 
exported from Surface Evolver, because they belong to the substrate plane, not 
to the drop surface.
"""
matrix = pd.read_csv("eg.txt",sep="\s+").to_numpy()

x = matrix[:,1]
y = matrix[:,2]
z = matrix[:,3]

x = x -0.5
y = y - 0.5

s = np.size(x)

X = np.array([])
#print(X)

Z = np.array([])
#print(Z)


for i in range(s):
    if y[i] == 0:
        X = np.append(X,x[i])
        Z = np.append(Z,z[i])
        
i = np.argsort(X)
X = X[i]
Z = Z[i]

plt.plot(X,Z, color = "blue", label = "Earth")

#INTERPOLATION SECTION
linear_interp = intp(X,Z)

#Here is the control of the  interpolated points, the last number in the 
#linspace defines how many interpolations will be done, and the 2 first numbers
#define the interval in witch the interpolation will occur.
xvalues = np.linspace(-0.995096, 0.995096, 3)

linear_results = linear_interp(xvalues)

ee = max(linear_results)

print(ee)

#plt.scatter(X,Z, color = "blue")
#plt.scatter(xvalues,linear_results, color = "blue")

#---------------------------------------

#STEP 1: Import data from surface evolver 

"""
OBS: make sure to delete vertices 9,10,11,12,17 and 18 from the .txt file 
exported from Surface Evolver, because they belong to the substrate plane, not 
to the drop surface.
"""
matrix = pd.read_csv("jg.txt",sep="\s+").to_numpy()

x = matrix[:,1]
y = matrix[:,2]
z = matrix[:,3]

x = x -0.5
y = y - 0.5

s = np.size(x)

X = np.array([])
#print(X)

Z = np.array([])
#print(Z)


for i in range(s):
    if y[i] == 0:
        X = np.append(X,x[i])
        Z = np.append(Z,z[i])
        
i = np.argsort(X)
X = X[i]
Z = Z[i]

plt.plot(X,Z,color = "orange", label = "Jupiter")

#INTERPOLATION SECTION
linear_interp = intp(X,Z)

#Here is the control of the  interpolated points, the last number in the 
#linspace defines how many interpolations will be done, and the 2 first numbers
#define the interval in witch the interpolation will occur.
xvalues = np.linspace(-0.995096, 0.995096, 3)

linear_results = linear_interp(xvalues)

ej = max(linear_results)

print(ej)

#plt.scatter(X,Z, color = "orange")
#plt.scatter(xvalues,linear_results, color = "orange")

#---------------------------------------

#---------------------------------------

#---------------------------------------

#FINAL SETTINGS

#Plot settings

plt.title('Gravity effects under the shape of drops')

#Title of x axis:
plt.xlabel('x axis')
#Title of y axis:
plt.ylabel('z axis')

plt.axes().set_aspect('equal')

plt.legend(loc='upper right', prop={'size': 7})

plt.xticks(np.linspace(-1.5,1.5,7))

plt.xlim(-1.5,1.5)
plt.ylim(0,0.8)

#ge = gravity effects
plt.savefig('Ege.svg')
