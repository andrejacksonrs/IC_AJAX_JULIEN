#The main goal of this script is make a graph with statistical analysis of the
#data envolved in a capillary experiment.

#This is script was created by André Jackson Ramos Simões, November 2019,
#using the software spyder from Anaconda Navigator.

#Version 2



#libraries used

#numpy is a library that have lots of mathematical tools (num from number)
import numpy as np

#matplotlib.pyplot allows commands to plot graphs
import matplotlib.pyplot as plt

#scipy allows us to make curve fittings (adjust the points to a certain curve)
#The optimize element of scipy is the one that will be used for fitting
import scipy
from scipy import optimize



#INSERTING VARIABLES SECTION:


#Definition of a vector containing the radius of the capillary tubes used in 
#the experiment:
r = np.array([0.5,0.75,1,1.5])

#OBS: The numpy library is used as np, and it is very useful to operate the
#vector mathematically.

#The height of the water after the capillary rise in the tube was measured 5
#times for the same tube, and each H bellow is a vector that contain that 5 
#values, for example, the first capillary tube (with a certain radius) had the 
#heights that are listed in H1.
H1 = np.array([32,31,30,30,25])
H2 = np.array([19.5,21,18,17,21])
H3 = np.array([13,12.5,14.5,13.5,13])
H4 = np.array([7.5,7,7,7,6.5])

# Let A be a vector. The command A.mean() gives the mean value of the elements
#of the vector A. Thus, the variables H1m, H2m, etc. below are defined as the 
#mean values of the measurements inside the vectors H1, H2, etc. 
H1m = H1.mean()
H2m = H2.mean()
H3m = H3.mean()
H4m = H4.mean()

#Definition of a vector containing the mean values of the Heights, with each
#coordinate corresponding to a specifical radius of capillary tube:
Hm = np.array([H1m,H2m,H3m,H4m])

# Let A be a vector. The command A.std() gives the standard deviation value of
#the elements of the vector A. Thus, the variables H1e, H2e, etc. below are 
#defined as the standard deviation values of the measurements inside the 
#vectors H1, H2, etc.
H1e = H1.std()
H2e = H2.std()
H3e = H3.std()
H4e = H4.std()

#Definition of a vector containing the standard deviation values of the 
#Heights, with each coordinate corresponding to a specifical radius of 
#capillary tube:
Herror = np.array([H1e,H2e,H3e,H4e])



#FITTING SECTION:


#The next command defines a function, it will be used as the fitting curve

def h(x,a):
    return a*pow(x,-1) 

#Because of Jurin's law, it is expected that the Height of water rise in the
#capillary tube to be inversely proportional to the radius of the tube. Then
#Height H is a function of the radius R in the form of H = a/R, a power relation
#That is why the fitting function has the form h(x) =  a*pow(x,-1) = a/x, where
#x is expected to be the radius of the capilarry tube.

#If is wanted to make a fit with another parameter b, indicating an offset of H
#The following function can be used by removin the hashtag before the commands
#bellow and changing h to h2 in all other commands below where h appears, beyond
#adding the parameter b where were only the parameter a as h(x,a) -> h2(x,a,b)
    
#def h2(x,a,b):
    #return a*pow(x,-1) + b
    
#To start the fitting process, it is necessary to guess what value the 
#parameters will have, then the next command is used for 1 parameter(in this 
#case the parameter a) (guess = [1] is equal to say that a = 1 is the guess)
guess = [1]

#if is wanted to use another parameter b, the following command can be used 
#just by taking out the hashtag sign in front of it, and putting a hashtag 
#sign in front of the guess = [1] command, deactivating it.

#guess = [1,1]

#Fitting command: scipy.optimize.curve_fit(function tha will be used as fit,
#x axis variable vector, y axis variable vector, guess used(in this case the
#guess was a=1 as the command above))
params, params_covariance = scipy.optimize.curve_fit(h, r, Hm, guess)
params

#Definition of a as the parameter used in the fitting method
a = params[0]
#If is wanted to fit the points with another parameter with the function h2,
#just taking out the hashtag of the following command will define the other
#parameter as b of h2(x,a,b):
#b = params[1]



#PLOT AND FORMATATION OF THE GRAPH



#To put a title in the graph, it is used the command:
plt.title('Tubos capilares: H(r)', fontsize = 15)

#To plot the points and the error bar, it is used the next command:
plt.errorbar(r,Hm,yerr=Herror, fmt='o', color = 'black', capsize = 5)

#Title of x axis:
plt.xlabel('r (mm)',fontsize = 12)

#Title of y axis:
plt.ylabel('H (mm)',fontsize = 12)

#To plot the fitted curve, one way is to define rc as a vector containing a
#large number of x values so the curve plotted will be very smooth(try to see
#it by changing 10000 to 5 in the command, for example). The next command 
#defines rc to be the vector that contains 10000 values equally spaced between
#0.5 and 1.5, so the plotted curve contains 10000 points and its connections.
rc = np.linspace(0.5,1.5,10000)

#Plot of the fitted curve:
plt.plot(rc, h(rc,a), color="blue", label = 'Fitting: H(r) = a/r')

plt.legend(fontsize = 12)

#If the h2 function was used, just adding the next command and remove the above
#command will do the fitted curve plot:
#plt.plot(rc, h2(rc,a,b), color="blue")

#Definition of how the x axis ticks of the graph will be, the next commands 
#establishes that it will go from 0.5 to 1.5, and in total will be
#11 ticks there are equally spaced: 
plt.xticks(np.linspace(0.5,1.5,11))

#Definition of how y axis ticks will be:
plt.yticks(np.linspace(0,30,7))

#Definition of the parameter a as a string variable to print in the next 
#command:
A = str(a)

#Prints the value of the parameter a, found in the fitting process:
print("the a coeficient is equal to:")
print(A)


#The same above described process, but if the parameter b was used:
#B = str(b)
#print("the b coeficient is equal to:")
#print(B)

#Save this graph in the folder of this script(in this case pdf, but it could be
#png or jpeg for example)
#plt.savefig('ct.svg')







