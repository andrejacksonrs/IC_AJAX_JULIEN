#The main goal of this script is make a graph with statistical analysis of the
#data envolved in a capillary experiment.

# This is script was created by Andr� Jackson Ramos Sim�es, November 2019




#libraries used

import numpy as np
import matplotlib.pyplot as plt
import scipy
from scipy import optimize



#INSERTING VARIABLES SECTION:



r = np.array([0.5,0.75,1,1.5])

H1 = np.array([32,31,30,30,25])
H2 = np.array([19.5,21,18,17,21])
H3 = np.array([13,12.5,14.5,13.5,13])
H4 = np.array([7.5,7,7,7,6.5])

H1m = H1.mean()
H2m = H2.mean()
H3m = H3.mean()
H4m = H4.mean()

Hm = np.array([H1m,H2m,H3m,H4m])

H1e = H1.std()
H2e = H2.std()
H3e = H3.std()
H4e = H4.std()

Herror = np.array([H1e,H2e,H3e,H4e])



#FITTING SECTION:



def h(x,a):
    return a*pow(x,-1) 

guess = [1]

params, params_covariance = scipy.optimize.curve_fit(h, r, Hm, guess)
params

a = params[0]



#PLOT AND FORMATATION OF THE GRAPH



plt.title('Capillary tubes of Marcos', fontsize = 15)

plt.errorbar(r,Hm,yerr=Herror, fmt='o', color = 'black', capsize = 5)

plt.xlabel('Radius of the capillary tube (mm)')

plt.ylabel('Height of water rise (mm)')

rc = np.linspace(0.5,1.5,10000)

plt.plot(rc, h(rc,a), color="blue")

plt.xticks(np.linspace(0.5,1.5,11))

plt.yticks(np.linspace(6,30,13))

A = str(a)

print("the a coeficient is equal to:")
print(A)

plt.savefig('capillarytubesofmarcos.jpeg')