# The Laplacian is equal to the curvature of the Surface

* [ ]  Add Images to Ilustrate the development

## Introduction

A Surface slightly deformed from a plane has an interesting prorprierty: The function
that describes it has a curvature that is equal to it's Laplacian. In this document,
the goal is to prove that, using calculus and differential geometry relating the
curvature of simple curves to the curvature of surfaces and the Laplacian.

## Definitions

#### 1. The Laplacian: 
The Laplacian of a function $`\,f(x_1,x_2,x_3, ...\,,x_n)\,`$
of n variables is denoted
by $`\,\nabla ^ 2 f\,`$ and it is the sum of it's n partial derivatives of
second order:

```math

\nabla ^ 2 f = \frac{\partial ^2 f}{\partial x_1^2} + \frac{\partial ^2 f}{\partial x_2^2} + ... + \frac{\partial ^2 f}{\partial x_n^2}

```


In particular, The Laplacian of a function of the form $`\,\zeta(x,y)\,`$ is denoted by 
$`\,\nabla ^ 2\zeta\,`$ and given by:

```math

\nabla ^ 2\zeta\ = \frac{\partial ^2 \zeta}{\partial x^2} + \frac{\partial ^2 \zeta}{\partial y^2}

```

#### 2. The Curvature of a 2D Curve: 

A 2D curve can be parametrized by a parameter $`\,t\,`$ and be defined by a vector
function in the form of:

```math

\vec{r} = (x(t),y(t))

```

Then, it's curvature $`\,k\,`$  is defined to be the derivative of the unit tangent vector $`\,\vec{T}\,`$
of the curve with respect to it's arc-lenght $`\,S\,`$, and it can be expressed as follows:

```math

k = \bigg|\bigg| \frac{d \vec{T}}{dS} \bigg|\bigg| = \frac{x'.y'' - x''.y'}{[(x')^2 + (y')^2]^{3/2}}

```

#### 3. The Curvature of a Surface: 

The curvature $`\,C\,`$ of a Surface defined by a function $`\,\zeta(x,y)\,`$ is defined by
the sum of two principal curvatures $`\,k_1\,`$ and $`\,k_2\,`$, i.e. , the curvature of the curve that appears
when a plane intersects the surface and the curvature of the curve that appears when
another plane, perpendicular to the first, intersects the surface:

```math

C = k_1 + k_2 

```

## Thesis and Development

#### 1. The Slightly deformed Hypothesis

Imagine a constant funtion like $`\,f(x) = 2\,`$ for example. Now imagine that this 
straight horizontal line is disturbed very slightly, described by a function in the form
of $`\,f(x) = 2 + h(x)\,`$ where $`\,h\,`$ is very very small. This function $`\,f(x)\,`$ 
is the object of study we are going to focus now.
To represent the "slightlyness" of this deformation, the following hipothesis can be done: 

```math

\frac{df}{dx} << 1

```

That means that the change in the curve height is very very small compared to one step
taken when we walk in the x axis direction. In other way it can be expressed as:

```math

df << dx

```
<div align="center">

| <img src=" Pictures/Slightly_Perturbed_Curve_Example.png" width = "500" > |
| ------ |
| Figure 1: An example of a slightly perturbed curve. |

</div>





#### 2. Thesis 

The cuve exhibits a curvature that is approximatedly the second order derivative:

```math

k = \frac{d^2f}{dx^2} 

```

#### 3. Demonstration

##### 3.1 Curve Development

The curve described above can be fully described by the following vector:

```math

\vec{r} = (x(t),y(t)) = (x,f(x))

```
Taking $`\,t = x\,`$

Now, to find the curvature, these quantities must be calculated:

```math

x' = \frac{dx}{dt} = \frac{dx}{dx} = 1 \newline
```
```math
x'' = \frac{d}{dt} (x') = \frac{d}{dx} (1) = 0 \newline
```
```math
y' = \frac{dy}{dt} = \frac{df}{dx} \newline
```
```math
y'' = \frac{d}{dt} (y') = \frac{d}{dx} \bigg(\frac{df}{dx}\bigg) = \frac{d^2f}{dx^2}

```

Thus, comes the curvature:

```math

k = \frac{x'.y'' - x''.y'}{[(x')^2 + (y')^2]^{3/2}}

= \frac{1.\frac{d^2f}{dx^2} - 0.\frac{df}{dx}}{\bigg[1^2 + \bigg(\frac{df}{dx}\bigg)^2\bigg]^{\frac{3}{2}}}
\approx \frac{\frac{d^2f}{dx^2}}{[1 + 0]^\frac{3}{2}} = \frac{d^2f}{dx^2}

```
Where the last approximation is due to the Hypothesis:

```math

\frac{df}{dx} << 1 \implies \bigg[ 1 + \bigg(\frac{df}{dx}\bigg)^2 \bigg] \approx [ 1 + 0 ]

```



##### 3.2 Surface Development

The surface considered $`\, \zeta (x,y) \,`$ is slightly perturbed just like in the curve hypothesis. That means that 
the slightly perturbed hypothesis for the surface are expressed as:

```math

\frac{\partial \zeta}{\partial x} << 1

```

```math

\frac{\partial \zeta}{\partial y} << 1

```

With this hypotheis and the definition of the curvature of a surface, considering now
the plane $`\,xz\,`$ to the curvature $`\,k_1\,`$ and the plane $`\,yz\,`$ to the curvature $`\,k_2\,`$


```math

C = k_1 + k_2 = \frac{\partial^2 \zeta}{\partial x^2} + \frac{\partial^2 \zeta}{\partial y^2} = \nabla ^2 \zeta

```





