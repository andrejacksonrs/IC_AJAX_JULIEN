# Derivation of the function that describes the contact line under the presence of one single heterogeneity

## André Jackson Ramos Simões
## Julien Chopin
## December, 2019

* [x]  Stablish the first steps
* [x]  Show the tiny gradient aproxximation of Area
* [x]  Calculate the variation of $`\Sigma`$
* [x]  Show the integration by parts step
* [x]  Show that $`\nabla^2\zeta = 0`$
* [ ]  Show that $`\nabla^2\zeta = Curvature \, of\,  the \, Surface`$
* [ ]  Show that by laplace's law $`\nabla^2\zeta = \bigg(\frac{\Delta p}{\gamma}\bigg)`$

The goal of this document is to show that the function that describes the 
contact line of a fluid in contact with a single heterogeneity is logarithmic in 
the shape described by the following equation:

```math
\zeta (y,z=0) = \zeta (y,0) = \frac{f}{\pi \gamma} \, ln\bigg(\frac{y}{r_0}\bigg)
```
### 0. Variational Calculus and the Surface Energy

In the study of surface tension and capillarity, there is a strong principle
that dictates how the shape of the surface is going to be: The energy of the 
surface must be a minimum. In this 0 section we will obtain a result of the
application of this principle to the case of a plane surface slightly perturbed
using variational calculus.

First, let's consider some start points:

*  The surface is defined by the graph of a function in the form of $`\zeta(x,y)`$;
*  The surface is slightly perturbed in such a way that the function $`\zeta(x,y)`$ has a tiny gradient: $`\,\,\nabla \zeta \approx 0`$ 
```math

 \frac{\partial h}{\partial x} << 1 \, \, and \, \, \frac{\partial h}{\partial y} << 1 \implies  \nabla h \approx 0  
``` 
*  The energy of the surface considered only depends on the capillary effects:     $`\,\,U = \gamma . \Sigma`$

    *  where:

    *  U = Surface Energy

    *  $`\gamma`$ = Surface Tension of the surface
    
    *  $`\Sigma`$ = Surface Area 
 *  The variation of a function f (denoted by: $`\, \delta f \, `$) must be zero in a minimum or maximum configuration, just like a derivative. In the case of the surface energy U, when U is a minimum $` \, \delta U = 0\,`$ must be true.
 *  The $`\delta`$ operator of variational calculus is linear just like a diferential operator, and it's proprieties are just like the differentiation ones. 
    
    


So, since $`\gamma`$ is a constant, to find the minimum energy is to find the minimum area of the surface, because they are proportional to each other. Thus because the function $`\zeta(x,y)`$ defines the surface, it's area can be
calculated as:

```math
 \Sigma = \iint_\Sigma \sqrt{1 + (\nabla \zeta)^2} \, dx \, dy 
```
But, because $`\,\,\nabla \zeta \approx 0\,\,`$, this approximation can be used: $`\,\,(1+x)^n \approx (1 + nx)\,\,`$ if $`\,\,x << 1\,\,`$, with $`\zeta `$ behaving just like $` x `$:

```math
 \Sigma = \iint_\Sigma [1 + (\nabla \zeta)^2]^{\frac{1}{2}} \, dx \, dy \approx \iint_\Sigma \bigg[1 + \frac{1}{2} (\nabla \zeta)^2\bigg] \, dx \, dy
```

Then, to find the minimum of the surface energy function U, variational calculus can be applied, $`\delta U`$ must be 0:

```math
\delta U = \delta(\gamma. \Sigma) = \gamma . \delta \Sigma \implies \delta U = 0 \leftrightarrow \delta \Sigma = 0
```

Thus, the problem can be reduced to find the conditions for $`\delta \Sigma = 0`$:

```math
\delta \Sigma = \delta \iint_\Sigma \bigg[1 + \frac{1}{2} (\nabla \zeta)^2\bigg] \, dx \, dy
= \iint_\Sigma \delta\bigg[1 + \frac{1}{2} (\nabla \zeta)^2\bigg] \, dx \, dy
= \iint_\Sigma \delta (1) + \delta\bigg[\frac{1}{2} (\nabla \zeta)^2\bigg] \, dx \, dy
```
Since 1 is a constant function, it's variation $`\delta(1) = 0`$, thus:

```math
= \iint_\Sigma \delta\bigg[\frac{1}{2} (\nabla \zeta)^2\bigg] \, dx \, dy
= \iint_\Sigma \frac{1}{2} \delta (\nabla \zeta)^2 \, dx \, dy
```
With the chain rule, comes that:
```math
= \iint_\Sigma \frac{1}{2} [2(\nabla \zeta).\delta(\nabla \zeta)] \, dx \, dy
= \iint_\Sigma (\nabla \zeta).\delta(\nabla \zeta)  \, dx \, dy
= \iint_\Sigma (\nabla \zeta).\nabla(\delta \zeta)  \, dx \, dy
```

```math
= \iint_\Sigma \bigg[\frac{\partial \zeta}{\partial x}  \frac{\partial (\delta \zeta)}{\partial x}\bigg] 
+ \bigg[\frac{\partial \zeta}{\partial y}  \frac{\partial (\delta \zeta)}{\partial y}\bigg]   \, dx \, dy
= \iint_\Sigma \frac{\partial \zeta}{\partial x}  \frac{\partial (\delta \zeta)}{\partial x} \, dx \, dy
+ \iint_\Sigma \frac{\partial \zeta}{\partial y}  \frac{\partial (\delta \zeta)}{\partial y}   \, dx \, dy
```

Now, iterating just the first integral, comes that:

```math
\iint_\Sigma \bigg[\frac{\partial \zeta}{\partial x}  \frac{\partial (\delta \zeta)}{\partial x}\bigg] = \int_y \bigg[ \int_x \frac{\partial \zeta}{\partial x}  \frac{\partial (\delta \zeta)}{\partial x}  \, dx \bigg] \, dy
```

The inside integral can be solved by integration by parts as follows:

```math
 \int_x \frac{\partial \zeta}{\partial x}  \frac{\partial (\delta \zeta)}{\partial x}  \, dx 
 = \int u \, dv = u.v - \int v \, du
```
with $` u = \frac{\partial \zeta}{\partial x}`$ and $`v = \delta \zeta`$, then:

```math
 \int_x \frac{\partial \zeta}{\partial x}  \frac{\partial (\delta \zeta)}{\partial x}  \, dx 
 = \int u \, dv = u.v - \int v \, du = \frac{\partial \zeta}{\partial x}.\delta \zeta -
 \int_x \delta \zeta . \frac{\partial^2 \zeta}{\partial x^2} \, dx
```

Going back to eq. (), comes that:

```math

\iint_\Sigma \bigg[\frac{\partial \zeta}{\partial x}  \frac{\partial (\delta \zeta)}{\partial x}\bigg]
= \int_y \bigg[ \frac{\partial \zeta}{\partial x}.\delta \zeta -
 \int_x \delta \zeta . \frac{\partial^2 \zeta}{\partial x^2} \, dx \bigg] \,  dy
 = \int_y \frac{\partial \zeta}{\partial x}.\delta \zeta \, dy - \int_y \int_x \delta \zeta . \frac{\partial^2 \zeta}{\partial x^2} \, dx \bigg] \,  dy

```

Using the tiny gradient approximation, $`\frac{\partial \zeta}{\partial x} \approx 0`$, the first integral approaches zero and:

```math

\iint_\Sigma \bigg[\frac{\partial \zeta}{\partial x}  \frac{\partial (\delta \zeta)}{\partial x}\bigg]

= - \int_y \bigg[ \int_x \delta \zeta . \frac{\partial^2 \zeta}{\partial x^2} \, dx \bigg] \,  dy
= -\iint_\Sigma \delta \zeta . \frac{\partial^2 \zeta}{\partial x^2} \, dx  \,  dy

```

Simillarly, for the second integral in eq. (), the result is:

```math

\iint_\Sigma \bigg[\frac{\partial \zeta}{\partial y}  \frac{\partial (\delta \zeta)}{\partial y}\bigg]

= -\iint_\Sigma \delta \zeta . \frac{\partial^2 \zeta}{\partial x^2} \, dx  \,  dy 
-\iint_\Sigma \delta \zeta . \frac{\partial^2 \zeta}{\partial y^2} \, dx  \,  dy

```


Thus, equation () can be simplified as:

```math

\iint_\Sigma \frac{\partial \zeta}{\partial x}  \frac{\partial (\delta \zeta)}{\partial x} \, dx \, dy
+ \iint_\Sigma \frac{\partial \zeta}{\partial y}  \frac{\partial (\delta \zeta)}{\partial y}   \, dx \, dy
= -\iint_\Sigma \delta \zeta . \frac{\partial^2 \zeta}{\partial y^2} \, dx  \,  dy
```

```math
= \iint_\Sigma \delta \zeta . \bigg( \frac{\partial^2 \zeta}{\partial x^2} + \frac{\partial^2 \zeta}{\partial y^2} \bigg) \, dx  \,  dy 
= \iint_\Sigma \delta \zeta . \nabla^2 \zeta \, dx \, dy
```


Now, using integration by parts in the first integral, comes that:

```math
\iint_\Sigma (\nabla \zeta).\nabla(\delta \zeta)  \, dx \, dy
= (\nabla \zeta) \delta \zeta - \iint_\Sigma \delta \zeta . \nabla^2 \zeta \, dx \, dy
```
Thus, the Hypotesis $`\,\,\nabla \zeta \approx 0`$ implies:

```math
(\nabla \zeta) \delta \zeta - \iint_\Sigma \delta \zeta . \nabla^2 \zeta \, dx \, dy
= - \iint_\Sigma \delta \zeta . \nabla^2 \zeta \, dx \, dy
```
Thus:

```math
\delta \Sigma = - \iint_\Sigma \delta \zeta . \nabla^2 \zeta \, dx \, dy = 0 \leftrightarrow \nabla^2 \zeta = 0
```

Since $`\delta \zeta `$ is not zero, besides it tends to be zero just like a differential.



### 1. Visualization of the problem

The first step to understand this problem can be the visualization of a fluid
drop in a substrate without heterogeneities, just like the
next image: 

<img src = "https://gitlab.com/jchopin/AjaxCapillar/uploads/05e5d50428da2ee8e3b91d3eee2ec693/PIERRE_S_FIGURE_DROP.png"  width = "500" >


Now imagine that $` \theta_E = 90º`$, i.e., the lateral faces of the drop are
now vertical, just like the next drawings:

<img src = "https://gitlab.com/jchopin/AjaxCapillar/uploads/30b97121edf895bd173257f2bbd66f56/DROP_90_DEGREES_CONTACT_ANGLE.png"  width = "500" >
<img src = "https://gitlab.com/jchopin/AjaxCapillar/uploads/056d4295d1479f98c3773bc21f7963fc/DROP_90_DEGREES_CONTACT_ANGLE_ZOOMED.png"  width = "500" >

That vertical surface may be disturbed by some external action, for example the
presence of a punctual heterogeneity like a grain of sand. This disturb may cause
a deformation in that surface, and to understand how the fluid behaves, we can
imagine that the external environment caused a sinusoidal deformation of our
surface just like the next drawings:

The surface initially vertical:

<img src = "https://gitlab.com/jchopin/AjaxCapillar/uploads/b593b88dd86227a0f21af31ebc73159c/DRAWING_VERTICAL_SURFACE_3D_DROP.png"  width = "800" >

Then Deformed:

<img src = "https://gitlab.com/jchopin/AjaxCapillar/uploads/4947e49f6f5908bb0651e8d3203e8e5d/DRAWING_VERTICAL_SURFACE_DEFORMED_SINUSOIDALLY_3D_DROP.png"  width = "800" >

<img src = "https://gitlab.com/jchopin/AjaxCapillar/uploads/c0217a3514de8cafd2cd8564f7bb8d38/DRAWING_SINUSOIDAL_SHAPE_OF_CONTACT_LINE.png"  width = "800" >

This surface is defined by the drawed field $` \zeta (y,z) = x(y,z)`$. Now,
physically there is an important Law of Capillarity that in this case implies 
that the curvature of that surface must be zero, it is called Laplace's Law. 
That fact then can be written as:

```math
  \frac{\partial^2\zeta}{\partial y^2} + \frac{\partial^2\zeta}{\partial z^2} = 
  0
```
And solving that equation, with the boundary condition of: (sinusoidal shape) 

```math
\zeta(y,0) = u_q \, . \, cos(qy)
```

comes a shape of :

```math
\zeta(y,z) = u_q \, . \, cos(qy) \, . \, e^{-qz}
```

That justifies the exponential shape drawed in the previous drawings of the 
sinusoidal deformation.

### 2. Attacking the heterogeneity problem

As seen in the item 1., the problem of knowing the shape of the surface deformed
can be attacked with the curvature equation that comes from Laplace's Law. The
full form of that law can be written as: 

```math
 \Delta p = \gamma \, . \, C = \gamma \bigg(\,\frac{\partial^2\zeta}{\partial y^2} + 
 \frac{\partial^2\zeta}{\partial z^2} \bigg)
```

In witch the $`\Delta p `$ provided by a point heterogeneity can be modeled as
$`\Delta p = f \, . \, \delta(x) \, . \, \delta(y)`$. By this way, the Laplace's
Law implies that:

```math
  \gamma \bigg(\,\frac{\partial^2\zeta}{\partial y^2} + 
 \frac{\partial^2\zeta}{\partial z^2} \bigg) = 
  f \, . \, \delta(x) \, . \, \delta(y) 
```

And for that equation the requested solution is in fact:

```math
\zeta (y,z) = \frac{f}{\pi \gamma} \, ln\bigg(\frac{r}{r_0}\bigg)
```

with 

```math
r² = y² + z²
```

and therefore

```math
\zeta (y,z=0) = \zeta (y,0) = \frac{f}{\pi \gamma} \, ln\bigg(\frac{y}{r_0}\bigg)
```

That is the shape of the contact line, i.e., the shape of the surface in contact
with the substrate $`(z=0)`$










