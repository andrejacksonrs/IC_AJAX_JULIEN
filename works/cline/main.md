---
Author: André Jackson Ramos Simões
Advisor: Julien Chopin
Date: July, 2020
From: Federal University of Bahia (UFBA)
---

# Contact Line Theory

### 1. Introduction

Wetting phenomena is around our lives everyday, whether on the bathroom floor wet by water, or the oil in a frying pan, for example. Inside the contents of these class of phenomena, there is one particular thing that may be interesting to study: The contact Line. When water wets a frying pan, for example, a line that divides 3 different medias(water, air, and frying pan) can be visualized, that is what we call the contact line, because it is in this line that the 3 medias are in contact to each other and divide themselves. The shape of this line can indicate heterogeneities like dirt in a surface, and because of that, studying it may be very useful to understand the effects of wetting on surfaces that are not perfectly smooth.

### 2. Investigation of the shape of the Contact Line

To begin the study of the contact line, two questions can be asked: 

"What exactly is the shape of the contact line ?" 

"What physical proprieties or interactions may change it's aspect ?"

Then, since the contact line is the intersection of the surface of the liquid (water, for example) with the surface of the substrate (a frying pan, for example) maybe we should study the surfaces first and then attack the contact line problem by intersecting them and finding the line. The study of surfaces in calculus will be very helpful and aside with the math involved in this problem, the surface phenomena part of Fluid Mechanics will help us too.

### 3. Surface of the Fluid 

When water is wetting a frying pan, for example, it exhibits a particular surface shape, kind of rounded in the boundaries. This shape is the one that contains the minimum energy, and because of that it is an equilibrium shape, i.e. the surface maintains it's position static. Using this principle of nature, that the energy of the surface must be a minimum, with variational calculus it is possible to derive the known Laplace's Law of capillarity:

```math

\Delta p = \gamma \, . \, C

```
Where:

$`\Delta p = p1 - p2 \,`$ is the difference of pressure between the 2 medias separated by the surface

C is the curvature of the surface

and $`\gamma`$ is the surface tension of the surface

<div align="center">


| <img src = "lp1.png" width = "500"> |
| ------ |
| Figure 1: Illustration of the pressure field in a system of 2 fluids divided by a surface. Laplace's pressure is equal to the difference of the written pressures p1 and p2, the curvature C is measured in the black point, between the pressure points (x1,y1,z1) and (x2,y2,z2) analyzed. |


</div>


### 4. Slightly Perturbed Surface

Now, imagine the surface of water in a very very quiet lake, it is almost a plane, right ? 

What if there is a very small perturbation on it in a way that the surface slightly changes ?

The surface in this conditions, considering only surface tension effects, is defined by a function of the kind: $`\, \zeta(x,y)\,`$ and it must satisfy Laplace's equation: $`\,\nabla ^2 \zeta = 0\,`$ because of the simple fact that the energy must be a minimum and it depends on the area of the surface. This Result is discussed in more details in the following link:

[Laplacian Equals To Zero](https://gitlab.com/jchopin/AjaxCapillar/-/blob/master/Contact%20Line%20Study/Slightly_Perturbed_Surface_With_only_Capillarity.md)

Beyond that, the laplacian of the function $`\,\zeta\,`$ is equal to the curvature of the surface in this case, because the principal curvatures of the surface slightly perturbed matches with it's partial second derivatives just like a slightly perturbed 2D curve has a curvature equals to it's second derivative. Again, this result is discussed in more detail in the following link:

[The Laplacian is equal to the curvature of the Surface](https://gitlab.com/jchopin/AjaxCapillar/-/blob/master/Contact%20Line%20Study/Laplacian_Equals_To_Curvature.md)

### 5. The Laplace's Formula with the Laplacian

Know that we have discussed in more details the math involved with slightly perturbed surfaces, the curvature of a surface has an interesting physical relation to the pressure, as talked above in the Laplace's Law. Now that we know that the curvature of slightly perturbed surfaces is equal to the laplacian, Laplace's Law exhibits a specific form:

```math

\Delta p = \gamma \, . \, C = \Delta p = \gamma \, . \, \nabla ^2 \zeta 

```

```math

\nabla ^2 \zeta = \bigg( \frac{\Delta p}{\gamma} \bigg) 

```
### 6. The Heterogeneity Problem

It's not very hard to see in nature water wetting surfaces and spreading until it stops and exhibits a shape like that:

<div align="center">


| <img src = "CLP/P1090184.JPG" width = "500"> |
| ------ |
| Figure 2:  Water wetting a frying pan.|


</div>

This image, for example, was taken throwing water in a dirty frying pan.

If we take a more zoomed look at it, we may see that in a part of the surface there are some bumps, that may indicate that there is one heterogeneity:

<div align="center">

| <img src = "CLP/z1.png" width = "500"> | <img src = "CLP/z2.png" width = "400"> |
| ------ | ------- |
| Figure 3:  The surface of the fluid zoomed. There are smooth parts of the surface more in the right side, and irregular ones in the left.| Figure 4: Irregularities in the surface that may represent heterogeneities in the substrate.| 

</div>

So, to model this problem, we may stablish a reference just like the next figure: 

<div align="center">

| <img src = "CLP/f1.png" width = "300"> | <img src = "CLP/fc2.png" width = "500"> |
| ------ |-|
| Figure 5:  The fluid boundary.| Figure 6: The fluid boundary with the referential xyz and the function $`\, \zeta(y,z) \, `$ represented by the blue arrows that describes the surface of the boundary.

</div>

Realizing that in this considered case, where the fluid touches the substrate with a 90 degree angle (contact angle = 90 degrees), the surface may be slightly perturbed from a plane. Thus, it is possible to say that
the laplacian $`\, \nabla ^2 \zeta  \,`$ is the curvature of the surface defined by $`\, \zeta (x,y) \,`$ .

Then, the pressure difference in the surface (laplace's pressure) may be calculated as ilustrated in the next figures:

<div align="center">

| <img src = "CLP/fl1.png" width = "500"> | <img src = "CLP/fh1.png" width = "500"> |
| ------ |-|
| Figure 7: Laplace's pressure and surface next to the contact line without heterogeneities.| Figure 8: Laplace's pressure and surface in contact with a single heterogeneities.|

</div>

Since the single heterogeneity may be very localized in a position (x,y), it is possible to model the pressure difference caused by it's presence by a dirac's delta function just like in the next equation:

```math

\Delta p = \gamma \nabla ^2 \zeta = \gamma \bigg( \frac{\partial ^2 \zeta}{\partial y^2} + \frac{\partial ^2 \zeta}{\partial z^2} \bigg) = f. \delta (x,y)

```

where the laplacian is taken just from the (y,z) coordinates because the surface is defined to be a function of the plane yz of the type x(y,z). Otherwise, the dirac's delta is a function of (x,y) because the heterogeneity is located in the xy plane.
The constant f is a number that represents the tiny force caused by heterogeneity under it's surroundings, because the integral of the dirac's delta of (x,y) on the xy plane is equal to 1 just like it follows:

```math

\iint_{z=0} f. \delta (x,y) \, dx \, dy = f \iint_{z=0} \delta (x,y) \, dx \, dy = f.1 = f 

```

OBS: the dimension of a dirac's delta is $`lenght ^{-1}`$ because $`\int \delta(x) dx = 1`$, i.e. 


```math

[\delta(x)].[dx] = [\delta(x)].[L] = 1 = dimentionless \implies [\delta(x)] = lenght ^{-1} \,\, since \,\, [dx] = lenght


```

Then, solving this partial differential equation, comes the solution:

``` math

\zeta (y,z) = \frac{f}{\pi . \gamma}.ln(\frac{\sqrt{y^2 + z^2}}{r_0})

```

witch solution is discussed in more details in the following link: 

![Solution of the Heterogeneity PDE]()

The value $`\, r_0 \,`$ is the cutoff radius of the heterogeneity. Taking $`\, z = 0 \,`$
in the solution, comes the contact line shape, i.e. , the intersection of the surface $`\, \zeta (y,z) \,`$
with the plane $`\, z = 0 \,`$:

``` math

\zeta (y,0) = \frac{f}{\pi . \gamma}.ln(\frac{\sqrt{y^2 + 0^2}}{r_0}) 

```

``` math

\zeta (y) = \frac{f}{\pi . \gamma}.ln(\frac{\, y \, }{\, r_0 \,}) 

```

### 7. Conclusion

Through concepts of differential geometry like curvature, and the minimization 
of surface energy through variational calculus, laplace's law of capillarity 
appears. Then, this law can be used to study the case of slightly perturbed 
surfaces, where the curvature of the surface is equal to the laplacian of the 
function that describes the surface. With the analysis done in this document, 
the use of these fundamental knowledges of math and physics it was possible to 
predict theoretically one equation that describes the contact line visualized in
many different contexts of our daily lives. This developed theory states that 
it's shape must be logarithmic, and to conclude this document an interesting way
is comparing this result to phenomena pictures just like the next figures can 
show:


an

### Appendix Surface Energy
### Surface of the Fluid and it's energy

The surface of a fluid is a weird place. Molecules of the fluid interact with each other and this is represented by a negative amount of energy. Then, because molecules of the surface have half of the interactions of the molecules in the volume of the fluid, surface molecules have greater energy than the volume ones. To understand this, an example may help us: 

Imagine a fluid where the volume has 6 interactions and then -6,0 eV of energy for example. Then, the molecules of the surface have -3 eV of energy because they have half less interactions than the volume ones. Thus, the surface of the fluid, i.e., the molecules of the surface of the fluid, have greater energy. Summing all the energies of all molecules in the surface we may get -1000 eV, and the volume energy may be -10.000 eV. The difference of the energy from the surface molecules energy to the volume molecules energy is called surface energy, in this case it would be 9000 eV. 

Thus, because interactions are represented by the energy of the molecules, surface molecules have greater energy than the 
