# Surface Slightly Perturbed

In the study of surface tension and capillarity, there is a strong principle
that dictates how the shape of the surface is going to be: The energy of the 
surface must be a minimum. In this 0 section we will obtain a result of the
application of this principle to the case of a plane surface slightly perturbed
using variational calculus.

First, let's consider some start points:

*  The surface is defined by the graph of a function in the form of $`\zeta(x,y)`$;
*  The surface is slightly perturbed in such a way that the function $`\zeta(x,y)`$ has a tiny gradient: $`\,\,\nabla \zeta \approx 0`$ 
```math

 \frac{\partial \zeta}{\partial x} << 1 \, \, and \, \, \frac{\partial \zeta}{\partial y} << 1 \implies  \nabla \zeta \approx 0  
``` 
*  The energy of the surface considered only depends on the capillary effects:     $`\,\,U = \gamma . \Sigma`$

    *  where:

    *  U = Surface Energy

    *  $`\gamma`$ = Surface Tension of the surface
    
    *  $`\Sigma`$ = Surface Area 
 *  The variation of a function f (denoted by: $`\, \delta f \, `$) must be zero in a minimum or maximum configuration, just like a derivative. In the case of the surface energy U, when U is a minimum $` \, \delta U = 0\,`$ must be true.
 *  The $`\delta`$ operator of variational calculus is linear just like a diferential operator, and it's proprieties are just like the differentiation ones. 
    
    


So, since $`\gamma`$ is a constant, to find the minimum energy is to find the minimum area of the surface, because they are proportional to each other. Thus because the function $`\zeta(x,y)`$ defines the surface, it's area can be
calculated as:

```math
 \Sigma = \iint_\Sigma \sqrt{1 + (\nabla \zeta)^2} \, dx \, dy 
```
But, because $`\,\,\nabla \zeta \approx 0\,\,`$, this approximation can be used: $`\,\,(1+x)^n \approx (1 + nx)\,\,`$ if $`\,\,x << 1\,\,`$, with $`\zeta `$ behaving just like $` x `$:

```math
 \Sigma = \iint_\Sigma [1 + (\nabla \zeta)^2]^{\frac{1}{2}} \, dx \, dy \approx \iint_\Sigma \bigg[1 + \frac{1}{2} (\nabla \zeta)^2\bigg] \, dx \, dy
```

Then, to find the minimum of the surface energy function U, variational calculus can be applied, $`\delta U`$ must be 0:

```math
\delta U = \delta(\gamma. \Sigma) = \gamma . \delta \Sigma \implies \delta U = 0 \leftrightarrow \delta \Sigma = 0
```

Thus, the problem can be reduced to find the conditions for $`\delta \Sigma = 0`$:

```math
\delta \Sigma = \delta \iint_\Sigma \bigg[1 + \frac{1}{2} (\nabla \zeta)^2\bigg] \, dx \, dy
= \iint_\Sigma \delta\bigg[1 + \frac{1}{2} (\nabla \zeta)^2\bigg] \, dx \, dy
= \iint_\Sigma \delta (1) + \delta\bigg[\frac{1}{2} (\nabla \zeta)^2\bigg] \, dx \, dy
```
Since 1 is a constant function, it's variation $`\delta(1) = 0`$, thus:

```math
= \iint_\Sigma \delta\bigg[\frac{1}{2} (\nabla \zeta)^2\bigg] \, dx \, dy
= \iint_\Sigma \frac{1}{2} \delta (\nabla \zeta)^2 \, dx \, dy
```
With the chain rule, comes that:
```math
= \iint_\Sigma \frac{1}{2} [2(\nabla \zeta).\delta(\nabla \zeta)] \, dx \, dy
= \iint_\Sigma (\nabla \zeta).\delta(\nabla \zeta)  \, dx \, dy
= \iint_\Sigma (\nabla \zeta).\nabla(\delta \zeta)  \, dx \, dy
```

```math
= \iint_\Sigma \bigg[\frac{\partial \zeta}{\partial x}  \frac{\partial (\delta \zeta)}{\partial x}\bigg] 
+ \bigg[\frac{\partial \zeta}{\partial y}  \frac{\partial (\delta \zeta)}{\partial y}\bigg]   \, dx \, dy
= \iint_\Sigma \frac{\partial \zeta}{\partial x}  \frac{\partial (\delta \zeta)}{\partial x} \, dx \, dy
+ \iint_\Sigma \frac{\partial \zeta}{\partial y}  \frac{\partial (\delta \zeta)}{\partial y}   \, dx \, dy
```

Now, iterating just the first integral, comes that:

```math
\iint_\Sigma \bigg[\frac{\partial \zeta}{\partial x}  \frac{\partial (\delta \zeta)}{\partial x}\bigg] = \int_y \bigg[ \int_x \frac{\partial \zeta}{\partial x}  \frac{\partial (\delta \zeta)}{\partial x}  \, dx \bigg] \, dy
```

The inside integral can be solved by integration by parts as follows:

```math
 \int_x \frac{\partial \zeta}{\partial x}  \frac{\partial (\delta \zeta)}{\partial x}  \, dx 
 = \int u \, dv = u.v - \int v \, du
```
with $` u = \frac{\partial \zeta}{\partial x}`$ and $`v = \delta \zeta`$, then:

```math
 \int_x \frac{\partial \zeta}{\partial x}  \frac{\partial (\delta \zeta)}{\partial x}  \, dx 
 = \int u \, dv = u.v - \int v \, du = \frac{\partial \zeta}{\partial x}.\delta \zeta -
 \int_x \delta \zeta . \frac{\partial^2 \zeta}{\partial x^2} \, dx
```

Going back to eq. (), comes that:

```math

\iint_\Sigma \bigg[\frac{\partial \zeta}{\partial x}  \frac{\partial (\delta \zeta)}{\partial x}\bigg]
= \int_y \bigg[ \frac{\partial \zeta}{\partial x}.\delta \zeta -
 \int_x \delta \zeta . \frac{\partial^2 \zeta}{\partial x^2} \, dx \bigg] \,  dy
 = \int_y \frac{\partial \zeta}{\partial x}.\delta \zeta \, dy - \int_y \int_x \delta \zeta . \frac{\partial^2 \zeta}{\partial x^2} \, dx \bigg] \,  dy

```

Using the tiny gradient approximation, $`\frac{\partial \zeta}{\partial x} \approx 0`$, the first integral approaches zero and:

```math

\iint_\Sigma \bigg[\frac{\partial \zeta}{\partial x}  \frac{\partial (\delta \zeta)}{\partial x}\bigg]

= - \int_y \bigg[ \int_x \delta \zeta . \frac{\partial^2 \zeta}{\partial x^2} \, dx \bigg] \,  dy
= -\iint_\Sigma \delta \zeta . \frac{\partial^2 \zeta}{\partial x^2} \, dx  \,  dy

```

Simillarly, for the second integral in eq. (), the result is:

```math

\iint_\Sigma \bigg[\frac{\partial \zeta}{\partial y}  \frac{\partial (\delta \zeta)}{\partial y}\bigg]

= -\iint_\Sigma \delta \zeta . \frac{\partial^2 \zeta}{\partial x^2} \, dx  \,  dy 
-\iint_\Sigma \delta \zeta . \frac{\partial^2 \zeta}{\partial y^2} \, dx  \,  dy

```


Thus, equation () can be simplified as:

```math

\iint_\Sigma \frac{\partial \zeta}{\partial x}  \frac{\partial (\delta \zeta)}{\partial x} \, dx \, dy
+ \iint_\Sigma \frac{\partial \zeta}{\partial y}  \frac{\partial (\delta \zeta)}{\partial y}   \, dx \, dy
= -\iint_\Sigma \delta \zeta . \frac{\partial^2 \zeta}{\partial y^2} \, dx  \,  dy
```

```math
= \iint_\Sigma \delta \zeta . \bigg( \frac{\partial^2 \zeta}{\partial x^2} + \frac{\partial^2 \zeta}{\partial y^2} \bigg) \, dx  \,  dy 
= \iint_\Sigma \delta \zeta . \nabla^2 \zeta \, dx \, dy
```


Now, using integration by parts in the first integral, comes that:

```math
\iint_\Sigma (\nabla \zeta).\nabla(\delta \zeta)  \, dx \, dy
= (\nabla \zeta) \delta \zeta - \iint_\Sigma \delta \zeta . \nabla^2 \zeta \, dx \, dy
```
Thus, the Hypotesis $`\,\,\nabla \zeta \approx 0`$ implies:

```math
(\nabla \zeta) \delta \zeta - \iint_\Sigma \delta \zeta . \nabla^2 \zeta \, dx \, dy
= - \iint_\Sigma \delta \zeta . \nabla^2 \zeta \, dx \, dy
```
Thus:

```math
\delta \Sigma = - \iint_\Sigma \delta \zeta . \nabla^2 \zeta \, dx \, dy = 0 \leftrightarrow \nabla^2 \zeta = 0
```

Since $`\delta \zeta `$ is not zero, besides it tends to be zero just like a differential.

