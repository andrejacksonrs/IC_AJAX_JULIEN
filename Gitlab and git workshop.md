# Gitlab and git workshop

## first:

1. Download Typora https://typora.io/

2. Download git https://git-scm.com/downloads

3. Create an account in Gitlab and download template project "workshop-project" https://gitlab.com/andrejacksonrs/workshop-project

4. Download python/anaconda https://www.anaconda.com/products/individual

5. Install mkdocs https://www.mkdocs.org/

6. Choose an image file to upload on the website

   

## In sequence we will:

1. Create a project in Gitlab
2. how to commit changes like upload files, edit files, etc.
3. issues
4. Clone repository to local
5. Work locally and then pushing up the changes to the remote repository
6. Work remotely and pulling up the changes of the project
7. Mkdocs website

## Final Results

- Project created, remotely and locally
- Know how to commit changes remotely and locally, submit issues
- Know how to push and pull from and to remote project
- Created their own website with mkdocs