```
---
Author: André Jackson Ramos Simões
Advisor: Julien Chopin
Date: August 2019 - July 2020
---
```


# Introduction to Capillarity and Wetting Phenomena with Surface Evolver

Hello ! My name is André Jackson Ramos Simões and this is the scientific initiation project that i participated with the supervision of prof. Julien Chopin in the period of 2019 - 2020. As a Physics undergraduate student at the Federal University of Bahia (UFBA), I had the pleasure of develop studies in the area of Capillarity and Wetting Phenomena studying fluids and how their surfaces behave in a world of small sizes, that is where these phenomena are more visible. To approach and overcome challenges related to these studies, an open-source spirit was taken. One of these challenges was to work remotely during quarantine, and the use of Gitlab and git was very helpful to maintain the interaction student-advisor in touch, besides other benefits. In this project, you can find works done studying these phenomena using tools such as **Python**, the simulator **Surface Evolver**, and this awesome version control tool that is **git** and **GitLab**. Also, there are a few tutorials about how to use these tools for processing data, like how to make a simple fit with python for example.

<img src = "img/phenomena.png" width = "800">                                                        

<b>Figure 1</b> - Capillarity and wetting phenomena in nature. (a) - The hydrophobic feathers of a bird creating a cloak of water (Colin Franks Photography, March, 2020). (b) - Insect walking on water due to surface tension forces that balances its weight (Animais que andam sobre as águas, iGUi Ecologia; August, 2018). (c) - A tiny drop on a leaf, this spherical shape is the least energy and thus an equilibrium shape for a drop (Free photo 82991660 © creativecommonsstockphotos - Dreamstime.com).



# Works

#### 1. Measuring water's surface tension with capillary tubes using Jurin's Law

When a capillary tube touches the surface of water as in video 1, a certain volume of the water rise on the tube because of its surface tension. So, this work shows and report a way to measure the surface tension of water by doing experiments of capillary rise with different glass capillary tubes.

<video controls = "controls" width = "500" > <source src = "img/caprise.mp4" video = "web/mp4"> <source> </video> <video controls = "controls" width = "500" > <source src = "img/capillary-rise.mp4" video = "web/mp4"> <source> </video>

<b> Videos 1 and 2 </b> - The first video (Video 1) shows the capillary rise phenomena in a thin glass tube. In video 2, there is an animation illustrating the abstraction of that phenomena.

[Detailed report - PDF file](https://gitlab.com/andrejacksonrs/IC_AJAX_JULIEN/-/blob/master/works/jurin/CAPILLARY_RISE_EXPERIMENT_-_JURIN_S_LAW.pdf)

[Python script used to process data, commented step by step](https://gitlab.com/andrejacksonrs/IC_AJAX_JULIEN/-/blob/master/works/jurin/capillary_experiment.py)

[Cleaner Python script - without comments](https://gitlab.com/andrejacksonrs/IC_AJAX_JULIEN/-/blob/master/works/jurin/Script_of_data_ananalysis_not_much_comments_version_1.py)



<iframe width="500" height = "500" src="https://www.youtube.com/embed/oYx7s5fR-PE" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<b>Video 3</b> - Presentation of work 1 at the <i>Congresso Virtual UFBA 2020</i> event in June.



#### 2. Theoretical Contact Line Study

<img src = "works/cline/CLP/fh1.png" width = "400">

**Figure 2** - One heterogeneity near the surface of a fluid can change its shape, and so the contact line, that divides the three media envolved: Air, Fluid, Substrate. 

In this study, a theoretical approach was taken to understand the equations present in capillarity phenomena, more specifically the basics of curvature studies and Laplace's formula that can predict the pressure difference between two media based on the curvature of the surface that divides these media.

[Detailed Report - .md file](https://gitlab.com/andrejacksonrs/IC_AJAX_JULIEN/-/blob/master/works/cline/main.md)



#### 3. Studying the shape of drops using Surface Evolver

<img src = "works/drops/img/realdrop_sedrop.png" width = 800>

**Figure 1** - A drop of water in a frying pan on the left, and its simulated version with Surface Evolver at the right.

Many physical quantities can interfere in how the shape of a drop would be like, and this variety of effects under the shape of a drop can be explored with Surface Evolver. In this work, Surface Evolver was used to simulate different settings of a drop, varying some parameters and seeing the result on its shape. 

[Detailed Report - .md file](https://gitlab.com/andrejacksonrs/IC_AJAX_JULIEN/-/blob/master/works/drops/Drops_Study.md)

In the next link is a folder that contains all the files used in this work. As a brief guide to this folder, in this work there are 4 parts: Cangle, volume, gravity and heterogen. They are all described in the detailed report, and inside each respective folder there are the Python scripts used to process their data, that may be a an interesting source to learn with:

[Folder of the project](https://gitlab.com/andrejacksonrs/IC_AJAX_JULIEN/-/tree/master/works/drops)



# Tutorials

#### Surface Evolver

[Understanding how Surface Evolver scripts are written - first contact with .fe scripts](https://gitlab.com/andrejacksonrs/IC_AJAX_JULIEN/-/blob/master/Tutorials/Surface%20Evolver/Surface%20Evolver%20Scripts%20Introduction.md)

[How Surface Evolver Evolutions Work - The logic behind with energy minimization and The Gradient Descent Method](https://gitlab.com/andrejacksonrs/IC_AJAX_JULIEN/-/blob/master/Tutorials/Surface%20Evolver/Surface%20Evolver%20Evolution%20Process%20Introduction.md)

#### Python

With these tutorials you can learn how to make graphs as beautiful as this one :D

<img src = "Tutorials/Python/nonlinearfit.png">

**Figure 4** - Simple non-linear fit done with Python through the software Spyder from the Anaconda Platform.

1. [Writting and Plotting 2D Data, with more detailed comments, perfect for begginers :D](https://gitlab.com/andrejacksonrs/IC_AJAX_JULIEN/-/blob/master/Tutorials/Python/Writting%20and%20Plotting%202D%20Data.py)

2. [Writting and Plotting 2D Data, without much comments](https://gitlab.com/andrejacksonrs/IC_AJAX_JULIEN/-/blob/master/Tutorials/Python/Writting%20and%20Plotting%202D%20Data%20Without%20Comments.py)

3. [Writting and Plotting 3D Data](https://gitlab.com/andrejacksonrs/IC_AJAX_JULIEN/-/blob/master/Tutorials/Python/Writting%20and%20Plotting%203D%20Data.py)

4. [Importing and Plotting 2D data](https://gitlab.com/andrejacksonrs/IC_AJAX_JULIEN/-/blob/master/Tutorials/Python/Importing%20and%20Plotting%202D%20data.py)

5. [Importing and Plotting 3D data](https://gitlab.com/andrejacksonrs/IC_AJAX_JULIEN/-/blob/master/Tutorials/Python/Importing%20and%20Plotting%203D%20data.py)

6. [Linear Fitting](https://gitlab.com/andrejacksonrs/IC_AJAX_JULIEN/-/blob/master/Tutorials/Python/Linear%20Fitting.py)

7. [Nonlinear Fitting](https://gitlab.com/andrejacksonrs/IC_AJAX_JULIEN/-/blob/master/Tutorials/Python/Nonlinear%20Fit.py)

8. [Other Python Tutorials can be found here](https://gitlab.com/andrejacksonrs/IC_AJAX_JULIEN/-/blob/master/Tutorials/Python)

OBS: They may not work in jupyter, pycharm or other python environments, because
they may have different interpretations of the same code. The following scripts
were written mainly for the Spyder environment, with the Anaconda platform. Despite of that,
a slightly change in the order of the commands in the script or other little modification may
solve this incompatibility problem, as long as the logic behind the commands are correct in relation to the environment used.

#### GitLab and git

[Tutorial step by step - Starting with git and GitLab](https://gitlab.com/andrejacksonrs/IC_AJAX_JULIEN/-/blob/master/Tutorials/Git_Gitlab/How_to_Download_and_Install_git.md)

[List of git commands](https://gitlab.com/andrejacksonrs/IC_AJAX_JULIEN/-/blob/master/Tutorials/Git_Gitlab/List_of_git_commands.md)

<iframe width="500" height= "500" src="https://www.youtube.com/embed/HIiaUZUdyU0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<b>Video 4</b> - Participation in round table about remote technologies and collaborative work to overcome quarantine issues and more on <i>Congresso Virtual UFBA 2020</i> event in May.

#### My Youtube Channel/Scientific Initiation (IC) Playlist

In the following playlist, you may find videos about the works/tutorials of the project and Capillarity and Wetting phenomena.

<iframe width="500" height="500" src="https://www.youtube.com/embed/videoseries?list=PLYFZxXAyAM1trPCrV0R9b9XpLg54X4oew" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# References

[1] DE GENNES, P. G.; BROCHARD-WYART, F.; QUÉRÉ, D. Capillarity and Wetting Phenomena: Drops, Bubbles, Pearls, Waves. New York: Springer-Verlag, 2004.

[2] LANDAU, L.D., LIFSHITZ, E. M. - Course of Theoretical Physics, Volume 6, Fluid Mechanics. 2 edition, 1987.

[3] LANG, S. Calculus of several variables, Addison-Wesley. Yale University, 1973.

[4] VAROQUAUX, G. et al. Scipy Lecture Notes: One document to learn numerics, science, and data with Python. 2015.

[5] BRAKKE, K. A. Surface evolver manual. Mathematics Department, Susquehanna Univerisity, Selinsgrove: 1994.

[6] BRAKKE, K. A. Surface evolver Workshop. Susquehanna Univerisity, Selinsgrove: 2004. Link: http://facstaff.susqu.edu/brakke/evolver/workshop/workshop.htm

[7] SHEWCHUK, J. R. An Introduction to the Conjugate Gradient Method Without the Agonizing Pain. School of Computer Science Carnegie Mellon University Pittsburgh: 1994.

[8] REFSNAES, R. H. A brief introduction to the Conjugate Gadient method. Fall 2009.

[9] WIKIPEDIA. Jurin's Law. URL link: https://en.wikipedia.org/wiki/Jurin's_law

[10] ROMÃO, G. Curso da Udemy: Git e GitHub - Básico e Intermediário:https://www.udemy.com/course/git-e-github-basico-e-intermediario


